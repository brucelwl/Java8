package com.lwl.concurrency.akka;

import akka.actor.UntypedAbstractActor;

/**
 * @author liwenlong - 2018/4/1 12:49
 */
public class Greeter extends UntypedAbstractActor {

    public enum Msg{
        GREET,DONE
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message == Msg.GREET){
            System.out.println(Thread.currentThread().getName()+" Hello world");
            this.getSender().tell(Msg.DONE,this.getSelf());
        }else{
            this.unhandled(message);
        }

    }
}
