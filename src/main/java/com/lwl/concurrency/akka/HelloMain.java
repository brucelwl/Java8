package com.lwl.concurrency.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.ConfigFactory;

/**
 * @author liwenlong - 2018/4/1 14:16
 */
public class HelloMain {

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("hello", ConfigFactory.defaultApplication());
        ActorRef helloWorld = system.actorOf(Props.create(HelloWorld.class), "helloWorld");
        System.out.println(helloWorld.path());
        system.terminate();
    }




}
