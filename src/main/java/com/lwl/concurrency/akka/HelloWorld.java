package com.lwl.concurrency.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;

/**
 * @author liwenlong - 2018/4/1 14:09
 */
public class HelloWorld extends UntypedAbstractActor {
    private ActorRef greeter;

    @Override
    public void preStart() throws Exception {
        System.out.println(HelloWorld.class.getName()+" preStart");
        greeter = this.getContext().actorOf(Props.create(Greeter.class), "greeter111");
        greeter.tell(Greeter.Msg.GREET,this.getSelf());
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        System.out.println(this.getClass().getName());
        if (message == Greeter.Msg.DONE){
            greeter.tell(Greeter.Msg.GREET,getSelf());
            getContext().stop(getSelf());
        }else{
            unhandled(message);
        }
    }
}



















