package com.lwl.concurrency.collection;

import java.util.concurrent.ThreadLocalRandom;

/**
 * <pre>
 * Java并发API提供指定的类在并发应用程序中生成伪随机。它是ThreadLocalRandom类，
 * 这是Java 7版本中的新类。它使用线程局部变量。每个线程希望以不同的生成器生成随机数，
 * 但它们是来自相同类的管理，这对程序员是透明的。
 * 在这种机制下，你将获得比使用共享的Random对象为所有线程生成随机数更好的性能。
 * </pre>
 * Created by liwenlong on 2018/1/17 11:06
 */
public class ThreadLocalRandomTest {

    public static class TaskLocalRandom implements Runnable {
        @Override
        public void run() {
            String name = Thread.currentThread().getName();
            for (int i = 0; i < 10; i++) {
                //ThreadLocalRandom的current()方法.这是一个静态方法,
                //它返回当前线程的ThreadLocalRandom对象,你可以使用这个对象生成随机数
                System.out.printf("%s: %d\n", name, ThreadLocalRandom.current().nextInt(100));
            }
        }
    }

    public static void main(String[] args) {
        Thread[] threads = new Thread[3];
        TaskLocalRandom task = new TaskLocalRandom();
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(task);
            threads[i].start();
        }
    }


}
