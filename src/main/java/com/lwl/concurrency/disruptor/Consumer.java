package com.lwl.concurrency.disruptor;

import com.lmax.disruptor.WorkHandler;

import java.time.LocalDateTime;

/**
 * @author liwenlong - 2018/4/1 15:12
 */
public class Consumer implements WorkHandler<PCData> {
    @Override
    public void onEvent(PCData pcData) throws Exception {
        Thread.sleep(4000);
        System.out.println(Thread.currentThread().getName() + ":Event:--" + pcData.getValue() + "--" + LocalDateTime.now().toString());
    }
}
