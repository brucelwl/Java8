package com.lwl.concurrency.disruptor;

/**
 * @author liwenlong - 2018/4/1 15:09
 */
public class PCData {

    private long value;

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
