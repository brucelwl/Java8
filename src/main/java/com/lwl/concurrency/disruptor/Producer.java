package com.lwl.concurrency.disruptor;

import com.lmax.disruptor.RingBuffer;

import java.nio.ByteBuffer;

/**
 * @author liwenlong - 2018/4/1 15:30
 */
public class Producer {

    private final RingBuffer<PCData> ringBuffer;

    public Producer(RingBuffer<PCData> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public void pushData(ByteBuffer byteBuffer){
        //grab the next sequence
        long sequence = ringBuffer.next();
        try {
            PCData pcData = ringBuffer.get(sequence);
            pcData.setValue(byteBuffer.getLong(0));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            ringBuffer.publish(sequence);
        }
    }




}
