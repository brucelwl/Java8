package com.lwl.concurrency.executor;

import java.util.concurrent.*;

/**
 * 执行者分离任务的启动和结果的处理 <br>
 * Created by liwenlong on 2018/1/11 15:27
 */
public class CompletionServiceTest {

    public static class ReportGenerator implements Callable<String> {
        private String sender;
        private String title;

        /**
         * 生成报表
         * @param sender
         * @param title
         */
        public ReportGenerator(String sender, String title) {
            this.sender = sender;
            this.title = title;
        }

        @Override
        public String call() throws Exception {
            try {
                Long duration = (long) (Math.random() * 10);
                System.out.printf("%s_%s: ReportGenerator: Generating a report during %d seconds\n", this.sender, this.title, duration);
                TimeUnit.SECONDS.sleep(duration);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String ret = Thread.currentThread().getName() + " " + sender + ": " + title;
            return ret;
        }
    }

    public static class ReportRequest implements Runnable {
        private String name;
        private CompletionService<String> service;

        public ReportRequest(String name, CompletionService<String> service) {
            this.name = name;
            this.service = service;
        }

        @Override
        public void run() {
            ReportGenerator reportGenerator = new ReportGenerator(name, "Report");
            service.submit(reportGenerator);
        }
    }

    public static class ReportProcessor implements Runnable {
        private CompletionService<String> service;
        private boolean end;

        public ReportProcessor(CompletionService<String> service) {
            this.service = service;
            this.end = false;
        }

        public void setEnd(boolean end) {
            this.end = end;
        }

        @Override
        public void run() {
            while (!end) {
                try {
                    Future<String> result = service.poll(20, TimeUnit.SECONDS);
                    if (result != null) {
                        String report = result.get();
                        System.out.printf("ReportReceiver: Report Received:%s\n", report);
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            System.out.printf("ReportSender: End\n");
        }
    }

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        CompletionService<String> service = new ExecutorCompletionService<>(executorService);
        ReportRequest faceRequest = new ReportRequest("face", service);
        ReportRequest onlineRequest = new ReportRequest("online", service);
        ReportProcessor processor = new ReportProcessor(service);
        Thread faceThread = new Thread(faceRequest);
        Thread onlineThread = new Thread(onlineRequest);
        Thread senderThread = new Thread(processor);
        System.out.println("Starting the threads");
        faceThread.start();
        onlineThread.start();
        senderThread.start();
        try {
            System.out.printf("Main: Waiting for the report generators.\n");
            faceThread.join();
            onlineThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Main: Shutting down the executor.\n");
        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        processor.setEnd(true);
        System.out.println("Main: Ends");
    }

}
