package com.lwl.concurrency.executor;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DelayTaskTest {
	
	static class Task implements Callable<String>{

		private String name;
		
		public Task(String name){
			this.name = name;
		}
		
		@Override
		public String call() throws Exception {
			System.out.println(name+" Starting at "+LocalDateTime.now());
			return "Hello world "+name;
		}
	}
	
	public static void main(String[] args) {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
		System.out.println("main:Starting at "+LocalDateTime.now());
		for (int i = 0; i < 5; i++) {
			Task task = new Task("Task-"+i);
			executor.schedule(task, i+1, TimeUnit.SECONDS);
		}
		executor.shutdown();
		try {
			executor.awaitTermination(1, TimeUnit.HOURS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Main: end at "+LocalDateTime.now());
	}

}
