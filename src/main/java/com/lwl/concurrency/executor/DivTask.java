package com.lwl.concurrency.executor;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author liwenlong - 2018/3/30 14:53
 */
public class DivTask implements Runnable {

    int a, b;

    public DivTask(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        //try {
            double re = a / b;
            System.out.println(re);
        //}catch (Exception e){
        //    e.printStackTrace();
        //}
    }

    public static void main(String[] args) {

        ThreadPoolExecutor pool = new TraceThreadPoolExecutor(0, 10, 0L,
                TimeUnit.SECONDS, new SynchronousQueue<>());

        for (int i = 0; i < 5; i++) {
            pool.execute(new DivTask(100, i));
        }

    }


}
