package com.lwl.concurrency.executor;

import org.junit.Test;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by liwenlong on 2018/1/16 17:02
 */
public class MyThreadFactoryTest {

    public static class MyThread extends Thread {
        private Date creationDate;
        private Date startDate;
        private Date finishDate;

        public MyThread(Runnable target, String name) {
            super(target, name);
            setCreationDate();
        }

        @Override
        public void run() {
            setStartDate();
            super.run();
            setFinishDate();
        }

        @Override
        public String toString() {
            StringBuilder buffer = new StringBuilder();
            buffer.append(getName());
            buffer.append(": ");
            buffer.append(" Creation Date: ");
            buffer.append(creationDate);
            buffer.append(" : Running time: ");
            buffer.append(getExecutionTime());
            buffer.append(" Milliseconds.");
            return buffer.toString();
        }


        public void setCreationDate() {
            creationDate = new Date();
        }

        public void setStartDate() {
            startDate = new Date();
        }

        public void setFinishDate() {
            finishDate = new Date();
        }

        public long getExecutionTime() {
            return finishDate.getTime() - startDate.getTime();
        }


    }

    public static class MyThreadFactory implements ThreadFactory {

        private int counter;
        private String prefix;

        public MyThreadFactory(String prefix) {
            this.counter = 1;
            this.prefix = prefix;
        }

        @Override
        public Thread newThread(Runnable r) {
            MyThread myThread = new MyThread(r, prefix + "_" + counter++);
            return myThread;
        }
    }

    public static class MyTask implements Runnable {

        @Override
        public void run() {
            try {
                System.out.println("任务开始");
                TimeUnit.SECONDS.sleep(2);
                System.out.println("任务结束");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //实现ThreadFactory接口来生成自定义线程
    @Test
    public void test() {
        MyThreadFactory myThreadFactory = new MyThreadFactory("MyThreadFactory");
        MyTask task = new MyTask();
        Thread thread = myThreadFactory.newThread(task);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Main: Thread information.\n");
        System.out.printf("%s\n", thread);
        System.out.printf("Main: End of the example.\n");
    }

    //在执行者对象中使用我们的 ThreadFactory
    @Test
    public void test2() {

        MyThreadFactory threadFactory = new MyThreadFactory("MyThreadFactory");
        ExecutorService executor = Executors.newCachedThreadPool(threadFactory);
        MyTask myTask = new MyTask();
        executor.submit(myTask);
        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Main: End of the program.\n");

    }

}


















