package com.lwl.concurrency.executor;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * 以上次任务执行的开始时间作为基准,固定延时执行,如果任务超过固定延时
 * 任务不会迭代,而是以上一次任务执行的时间为新的固定延时,
 * 如果任务执行时间降到首次指定的时延下,还是以上次任务的执行时间为固定时延
 *
 * 如果任务遇到异常,那么后续的所有任务都会停止调用,因此必须保证异常被及时处理
 * Created by liwenlong on 2018/1/16 17:41
 */
public class ScheduledExecutorDemo {

    public static void main(String[] args) {

        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(1);

        scheduledExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    int sleep = ThreadLocalRandom.current().nextInt(10) * 1000;
                    System.out.println(Thread.currentThread().getName()+" 开始 "+ LocalDateTime.now().toString()+" sleep "+sleep);
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },0,3, TimeUnit.SECONDS);


    }






}
