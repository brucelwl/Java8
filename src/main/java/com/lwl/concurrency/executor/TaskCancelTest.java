package com.lwl.concurrency.executor;

import java.util.concurrent.*;

public class TaskCancelTest {

	static class Task implements Callable<String> {

		@Override
		public String call() throws Exception {
			int a = 1;
			while (a==1) {
				System.out.println("Task:Test");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return Thread.currentThread().getName();
		}
		
	}
	
	public static void main(String[] args) {
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		Task task = new Task();
		System.out.println("Main:executing the task");
		Future<?> submit = executor.submit(task);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main:canceling the task");
		boolean cancel = submit.cancel(true);
		System.out.println("任务被取消:"+cancel);
		System.out.println("Main:Canceled:"+submit.isCancelled());
		System.out.println("Main:Done:"+submit.isDone());
		System.out.println(executor.getPoolSize());
		System.out.println(executor.getCompletedTaskCount());
		System.out.println(executor.getActiveCount());
		System.out.println(executor.getTaskCount());
		executor.shutdown();
	}

}
