package com.lwl.concurrency.forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.TimeUnit;

/**
 * Fork/Join框架（五）在任务中抛出异常<br>
 * Created by liwenlong on 2018/1/15 16:00
 */
public class TaskExceptionTest {
    public static class Task extends RecursiveTask<Integer>{
        private static final long serialVersionUID = 1602317274842016767L;

        @Override
        protected Integer compute() {
            System.out.println("compute  run...");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 10 / 0;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Task task = new Task();
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.execute(task);

        forkJoinPool.shutdown();

        //forkJoinPool.awaitTermination(1, TimeUnit.DAYS);

        if (task.isCompletedAbnormally()){
            System.out.println("任务出现异常");
            System.out.println(task.getException());
        }
        task.join();


    }











}
