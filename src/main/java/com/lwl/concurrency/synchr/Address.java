package com.lwl.concurrency.synchr;

/**
 * @author liwenlong - 2018/3/28 10:00
 */
public class Address{

    private int code;
    private String province;
    private String city;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{" +
                "code=" + code +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
