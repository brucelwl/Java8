package com.lwl.concurrency.synchr;

import java.util.concurrent.locks.ReentrantLock;

/**
 * <pre>
 * 大多数情况下,锁的申请都是非公平的
 * 公平锁它会按照时间的先后顺序,保证先到者先得,
 * 公平锁的一大特点是:不会产生饥饿现象,实现公平锁需要维护一个有序队列
 * 因此公平锁的实现成本比较高,性能相对也非常低下
 * </pre>
 * 
 * @author bruce - 2018/03/27 20:31:41
 */
public class FairReentrantLockTest {
	public ReentrantLock fairLock = new ReentrantLock(true);

	public void run() {
		try {
			fairLock.lock();
			System.out.println(Thread.currentThread().getName() + ",获得锁");
		} finally {
			fairLock.unlock();
		}
	}

	public static void main(String[] args) {
		FairReentrantLockTest r1 = new FairReentrantLockTest();
		Thread t1 = new Thread(r1::run, "thread-t1");
		Thread t2 = new Thread(r1::run, "thread-t2");
		t1.start();
		t2.start();
	}

}
