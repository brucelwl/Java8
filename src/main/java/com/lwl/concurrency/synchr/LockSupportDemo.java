package com.lwl.concurrency.synchr;

import java.util.concurrent.locks.LockSupport;

/**<pre>
 * LockSupport是一个非常方便的线程阻塞工具,它可以在线程内任意位置让线程阻塞
 * 和thread.suspend()相比,弥补了由于resume()在前发生,导致suspend()一直处于阻塞,Runnable且未释放cpu资源状态
 * LockSupport.park(); 阻塞当前线程,
 * LockSupport.unpark(thread); 释放指定的阻塞线程,即使在park()之前调用,也有效
 * LockSupport.unpark(thread)只作用于于下一次park(),
 * 如果同时调用了多次调用了park(),调用多次unpark()可能只会作用于第一个park(),剩下的park()会继续阻塞,可能系统就无法正常退出了
 * </pre>
 * @author liwenlong - 2018/3/28 17:10
 */
public class LockSupportDemo {

    public static final Object obj = new Object();
    static ChangeObjectThread t1 = new ChangeObjectThread("t1");
    static ChangeObjectThread t2 = new ChangeObjectThread("t2");

    static class ChangeObjectThread extends Thread{

        public ChangeObjectThread(String name) {
            super(name);
        }

        @Override
        public void run() {
                System.out.println("in 1 "+this.getName());
                LockSupport.park();
            System.out.println("park 2");
                LockSupport.park();
            System.out.println("park 3");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        t1.start();
        Thread.sleep(100);
        t2.start();
        LockSupport.unpark(t1);
        LockSupport.unpark(t2);
        //Thread.sleep(1000);
        LockSupport.unpark(t1);
        LockSupport.unpark(t2);
        t1.join();
        t2.join();
    }




}
