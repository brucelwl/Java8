package com.lwl.concurrency.synchr;

import java.util.concurrent.locks.LockSupport;

/**
 * <pre>
 *  LockSupport.park(); 支持中断影响,但是不会抛出异常
 *  Thread.interrupted()可以判断当前线程是否被中断
 * </pre>
 *
 * @author liwenlong - 2018/3/28 17:10
 */
public class LockSupportInterruptDemo {

    static ChangeObjectThread t1 = new ChangeObjectThread("t1");
    static ChangeObjectThread t2 = new ChangeObjectThread("t2");

    static class ChangeObjectThread extends Thread {

        public ChangeObjectThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            System.out.println("in " + this.getName());
            LockSupport.park();
            if (Thread.interrupted()) {
                System.out.println(this.getName() + " 被中断...");
            }
            System.out.println(this.getName()+" 运行结束...");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        t1.start();
        Thread.sleep(100);
        t2.start();
        Thread.sleep(2000);
        LockSupport.unpark(t2);

        Thread.sleep(2000);
        t1.interrupt();

    }


}
