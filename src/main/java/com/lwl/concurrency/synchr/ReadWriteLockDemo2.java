package com.lwl.concurrency.synchr;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**<pre>
 * 如果在系统中,读操作次数远远大于写操作,则读写锁就可以发挥最大的功效,提升系统性能
 * 注意这里的 value 是一个基本数据类型int,属于值传递,
 * 所以在读线程先执行的情况下,写线程写完以后,线程读取的数据并没有被改变,(不可变对象也不会被改变)
 * 如果是非不可变对象的引用类型,那么写线程写完以后,之前读线程读取到的值被改变
 * 解决办法: 读线程的读取的返回值采用对象深拷贝的方式解决问题.
 * </pre>
 * @author liwenlong - 2018/3/28 9:09
 */
public class ReadWriteLockDemo2 {
    private static final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static final Lock readLock = readWriteLock.readLock();
    private static final Lock writeLock = readWriteLock.writeLock();
    private static UserInfo userInfo = new UserInfo();

    public UserInfo handleRead(Lock lock) throws InterruptedException {
        try {
            lock.lock(); //模拟读操作,读操作越耗时,写锁的优势就越明显
            Thread.sleep(1000);
            //return userInfo;
            return userInfo.fullCopy(); //深拷贝
        } finally {
            lock.unlock();
        }
    }

    public void handleWrite(Lock lock, int index) throws InterruptedException {
        try {
            lock.lock(); //模拟写操作
            Thread.sleep(1000);
            userInfo.setAge(index);
            userInfo.setName(Thread.currentThread().getName());
            Address address = new Address();
            address.setCode(Thread.currentThread().hashCode());
            address.setProvince(Thread.currentThread().getName());
            userInfo.setAddress(address);
            System.out.println(lock.getClass().getName()+",写入:"+index);
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ReadWriteLockDemo2 demo = new ReadWriteLockDemo2();
        //数据初始化
        userInfo.setName("bruceli");
        userInfo.setAge(12);
        Address address = new Address();
        address.setProvince("上海");
        address.setCity("上海");
        address.setCode(1002);
        userInfo.setAddress(address);

        Runnable readRunnable = () -> {
            try {
                UserInfo o = demo.handleRead(readLock);
                System.out.println(Thread.currentThread().getName()+"-读锁读取:" + o);
                Thread.sleep(4000);
                System.out.println(Thread.currentThread().getName()+"-再次打印数据:" + o);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        for (int i = 0; i < 5; i++) {
            new Thread(readRunnable).start();
        }

        for (int i = 18; i < 20; i++) {
            new Thread(new WriteRunnable(demo, i)).start();
            Thread.sleep(4500);
        }

        for (int i = 0; i < 5; i++) {
            new Thread(readRunnable).start();
        }
    }

    static class WriteRunnable implements Runnable {
        ReadWriteLockDemo2 demo;
        int index;

        public WriteRunnable(ReadWriteLockDemo2 demo, int index) {
            this.demo = demo;
            this.index = index;
        }

        @Override
        public void run() {
            try {
                demo.handleWrite(writeLock, index);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
