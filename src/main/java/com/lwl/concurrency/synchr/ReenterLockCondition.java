package com.lwl.concurrency.synchr;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
/**
 * 重入锁的好搭档,Condition条件
 * @author bruce - 2018/03/27 21:39:22
 */
public class ReenterLockCondition implements Runnable {

	public static ReentrantLock lock = new ReentrantLock();
	public static Condition condition = lock.newCondition();

	@Override
	public void run() {
		lock.lock();
		try {
			condition.await();
			System.out.println("Thread is going on");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		ReenterLockCondition c = new ReenterLockCondition();
		Thread t1 = new Thread(c);
		t1.start();
		Thread.sleep(2000);
		//通知线程t1继续执行
		lock.lock();
		condition.signal();
		//如果省略下面的一行,即使调用signal()方法,condition.await()
		//也无法继续执行,因为无法获得锁
		lock.unlock();

	}

}
