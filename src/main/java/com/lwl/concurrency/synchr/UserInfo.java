package com.lwl.concurrency.synchr;

import com.lwl.utils.ProtostuffUtil;
import org.springframework.beans.BeanUtils;

/**
 * @author liwenlong - 2018/3/28 9:40
 */
public class UserInfo {
    private int age;
    private String name;
    private Address address;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public UserInfo fullCopy(){

        /*byte[] serializer = ProtostuffUtil.serializer(this);
        return ProtostuffUtil.deserializer(serializer, UserInfo.class);*/

        Address address = new Address();
        BeanUtils.copyProperties(this.getAddress(),address);
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(this,userInfo);
        userInfo.setAddress(address);
        return userInfo;

        //return userInfo;
        /*try {
            return (UserInfo)this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }*/
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}
