package com.lwl.concurrency.thread;

/**
 *
 *
 *
 * @author liwenlong - 2018/3/26 14:28
 */
public class AccountingVol implements Runnable {
    static AccountingVol instance = new AccountingVol();
    volatile int i = 0;

    public AccountingVol() {
        System.out.println("AccountingVol construct init...");
    }
    public synchronized void increase() {
        System.out.println(Thread.currentThread().getName());
        i++;
    }
    @Override
    public void run() {
        //synchronized(this){
            System.out.println(Thread.currentThread().getName()+"start...");
            for (int j = 0; j < 100000000; j++) {
                 increase();
                 //i++;
            }
            System.out.println(Thread.currentThread().getName()+"end...");
        //}
    }
    public static void main(String[] args) throws InterruptedException {
        new AccountingVol().test1();
    }
    private void test1() throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread thread1 = new Thread(instance);
        Thread thread2 = new Thread(instance);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println(instance.i);
        System.out.println(System.currentTimeMillis() - start);
    }
}
