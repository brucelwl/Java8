package com.lwl.concurrency.thread;

import java.util.concurrent.TimeUnit;

/**
 * 当一个Java应用内,只有守护线程时,Java虚拟机就自然退出
 * @author liwenlong - 2018/3/25 23:17
 */
public class DaemonDemo {

    public static class DaemonT extends Thread{
        @Override
        public void run() {
            while(true){
                System.out.println("I am alive");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static void main(String[] args) throws InterruptedException {
        DaemonT daemonT = new DaemonT();
        daemonT.setDaemon(true);
        daemonT.start();

        Thread.sleep(2000);
    }




}
