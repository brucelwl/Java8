package com.lwl.concurrency.thread;

/**在一些书籍说在虚拟机server模式下,ReaderThread线程中的while循环永远不会退出,因为在
 * 主线程中修改变量的值,ReaderThread线程是无法看到的,解决办法是使用volatile关键字
 * 但是经过实现表明,在虚拟机server模式下不用volatile关键字也能看到ready变量值的改变,可能是新版的虚拟机对此做了优化
 * 开发过程中建议还是加上volatile关键字
 * @author liwenlong - 2018/3/26 12:43
 */
public class NoVisibility {
    private volatile static boolean ready;
    private static int number;

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            System.out.println(ready);
            while (!ready) {
                System.out.println(ready);
                System.out.println(number);
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new ReaderThread().start();
        Thread.sleep(1000);
        number = 520;
        ready = true;
        Thread.sleep(20000);
    }


}
