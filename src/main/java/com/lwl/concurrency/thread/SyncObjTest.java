package com.lwl.concurrency.thread;

/**
 *
 * @author liwenlong - 2018/3/26 18:19
 */
public class SyncObjTest {
	
	public static void main(String[] args) {
		SyncObj syncObj = new SyncObj();
		new Thread(syncObj::syncMethod1).start();
		new Thread(syncObj::syncMethod2).start();

        new Thread(syncObj::syncObjMethod1).start();
        new Thread(syncObj::syncObjMethod2).start();

		/*new Thread(syncObj::noSyncMethod1).start();
		new Thread(syncObj::noSyncMethod2).start();
		new Thread(SyncObj::staticSyncMethod1).start();
		new Thread(SyncObj::staticSyncMethod2).start();*/
	}
	
	


}
