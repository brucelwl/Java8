package com.lwl.concurrency.thread;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 使用线程的stop()方法导致数据不一致的问题
 *
 * @author liwenlong - 2018/3/25 15:12
 */
public class ThreadStopTest {

    private static final User user = new User();

    private static AtomicInteger count = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
        new ReadObjectThread().start();
        for (int i = 0; i < 100; i++) {
            ChangeObjectThread thread = new ChangeObjectThread();
            thread.start();
            Thread.sleep(10);
            thread.stop();
        }
        System.out.println(count.get());
    }

    public static class ChangeObjectThread extends Thread {
        @Override
        public void run() {
            //while (true) {
                synchronized (user) {
                    int v = (int) (System.currentTimeMillis() / 1000);
                    user.setId(v);
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    user.setName(String.valueOf(v));
                    count.getAndAdd(1);
                }
                System.out.println(this.getState() + " " + Thread.currentThread().getName());
              //  Thread.yield();
            //}
        }
    }

    public static class ReadObjectThread extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (user) {
                    if (user.getId() != Integer.parseInt(user.getName())) {
                        System.out.println(user.toString());
                    }
                }
                Thread.yield();
            }
        }
    }

    public static class User {
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public User() {
            this.id = 0;
            this.name = "0";
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }


}
