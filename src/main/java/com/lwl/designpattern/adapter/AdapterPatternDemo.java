package com.lwl.designpattern.adapter;

/**
 * @author liwenlong - 2018/5/23 9:30
 */
public class AdapterPatternDemo {

    public static void main(String[] args) {

        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play( "beyond the horizon.mp3");
        audioPlayer.play("alone.mp4");
        audioPlayer.play( "far far away.vlc");
        audioPlayer.play( "mind me.avi");

    }




}
