package com.lwl.designpattern.adapter;

/**
 * @author liwenlong - 2018/5/23 9:27
 */
public interface AdvancedMediaPlayer {
    public void playVlc(String fileName);

    public void playMp4(String fileName);
}
