package com.lwl.designpattern.adapter;

/**
 * @author liwenlong - 2018/5/23 9:29
 */
public class AudioPlayer implements MediaPlayer {

    private MediaAdapter mediaAdapter = new MediaAdapter();

    @Override
    public void play(String fileName) {

        if (fileName.endsWith(".mp3")){
            System.out.println("Playing mp3 file. Name: "+ fileName);
        }else{
            mediaAdapter.play(fileName);
        }

    }
}
