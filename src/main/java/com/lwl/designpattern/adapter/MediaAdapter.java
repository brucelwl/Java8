package com.lwl.designpattern.adapter;

/**
 * @author liwenlong - 2018/5/23 9:31
 */
public class MediaAdapter implements MediaPlayer {
   private Mp4Player mp4Player;
   private VlcPlayer vlcPlayer;

    public MediaAdapter() {
        mp4Player = new Mp4Player();
        vlcPlayer = new VlcPlayer();
    }

    @Override
    public void play(String fileName) {
        if (fileName.endsWith(".mp4")) {

            mp4Player.playMp4(fileName);

        } else if (fileName.endsWith(".vlc")) {

            vlcPlayer.playVlc(fileName);

        }else{

            System.out.println("Invalid media. "+ fileName + " format not supported");

        }
    }
}
