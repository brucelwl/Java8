package com.lwl.designpattern.adapter;

/**
 * @author liwenlong - 2018/5/23 9:27
 */
public interface MediaPlayer {

    public void play(String fileName);

}
