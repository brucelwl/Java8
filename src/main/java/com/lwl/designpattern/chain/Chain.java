package com.lwl.designpattern.chain;

import java.util.List;

/**责任链模式
 * @author liwenlong - 2018/5/22 22:07
 */
public class Chain {

    private List<ChainHandler> handlers;
    private int index = 0;
    public Chain(List<ChainHandler> handlers){
        this.handlers = handlers;
    }

    public void process(){
        if (index >= handlers.size()){
            return;
        }
        handlers.get(index++).execute(this);
    }


}
