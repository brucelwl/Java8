package com.lwl.designpattern.chain;

/**
 * @author liwenlong - 2018/5/22 22:07
 */
public abstract class ChainHandler {

    public void execute(Chain chain){
        handle();
        chain.process();
    }

    protected abstract void handle();


}
