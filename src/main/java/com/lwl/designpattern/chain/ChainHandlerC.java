package com.lwl.designpattern.chain;

/**
 * @author liwenlong - 2018/5/22 22:07
 */
public class ChainHandlerC extends ChainHandler {

    @Override
    protected void handle() {
        System.out.println("handle by chain C");
    }


}
