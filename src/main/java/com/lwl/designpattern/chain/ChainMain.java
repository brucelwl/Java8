package com.lwl.designpattern.chain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author liwenlong - 2018/5/22 22:21
 */
public class ChainMain {


    public static void main(String[] args) {

        List<ChainHandler> handlers = Arrays.asList(new ChainHandlerA(),
                new ChainHandlerB(),
                new ChainHandlerC());

        Chain chain = new Chain(handlers);

        chain.process();

    }




}
