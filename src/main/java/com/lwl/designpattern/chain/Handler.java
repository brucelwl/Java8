package com.lwl.designpattern.chain;

/**
 * @author liwenlong - 2018/5/22 21:51
 */
public abstract class Handler {

    private Handler nextHandler;

    public Handler getNextHandler() {
        return nextHandler;
    }

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public void execute() {
        handle();
        if (nextHandler != null) {
            nextHandler.execute();
        }
    }

    protected abstract void handle();


}
