package com.lwl.designpattern.chain;

/**
 * @author liwenlong - 2018/5/22 21:58
 */
public class HandlerB extends Handler {
    @Override
    protected void handle() {
        System.out.println("Handler by B");
    }
}
