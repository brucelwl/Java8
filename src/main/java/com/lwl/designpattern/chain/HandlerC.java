package com.lwl.designpattern.chain;

/**
 * @author liwenlong - 2018/5/22 21:58
 */
public class HandlerC extends Handler {
    @Override
    protected void handle() {
        System.out.println("Handler by C");
    }
}
