package com.lwl.designpattern.chain;

/**
 * @author liwenlong - 2018/5/22 21:57
 */
public class HandlerMain{

    public static void main(String[] args) {
        HandlerA handlerA = new HandlerA();
        HandlerB handlerB = new HandlerB();
        HandlerC handlerC = new HandlerC();

        handlerA.setNextHandler(handlerB);
        handlerB.setNextHandler(handlerC);

        handlerA.execute();

    }

}
