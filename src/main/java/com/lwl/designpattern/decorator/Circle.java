package com.lwl.designpattern.decorator;

/**
 * Created by liwenlong on 2018/1/19 17:59
 */
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Shape: Circle");
    }
}
