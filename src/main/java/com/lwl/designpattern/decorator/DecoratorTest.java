package com.lwl.designpattern.decorator;

import org.junit.Test;

/**<pre>
 * 装饰者模式: 可以有效分离性能组件和功能组件,从而提升模块的可维护性并增加模块的复用性
 * 优点: 装饰类和被装饰类可以独立发展，不会相互耦合，
 * 装饰模式是继承的一个替代模式，装饰模式可以动态扩展一个实现类的功能。
 * 缺点: 多层装饰比较复杂
 * </pre>
 * Created by liwenlong on 2018/1/19 16:30
 */
public class DecoratorTest {

    @Test
    public void test1(){
        PacketDecorator pc = new PacketHttpHeaderCreator(
                new PacketHtmlHeaderCreator(
                        new PacketBodyCreator()));

        System.out.println(pc.handleContent());
    }

    @Test
    public void test2(){
        Shape circle = new Circle();
        System.out.println("circle with normal border");
        circle.draw();

        Shape redCircle = new RedShapeDecorator(circle);
        System.out.println("\nCircle of red border");
        redCircle.draw();

        Shape redRectangle = new RedShapeDecorator(new Rectangle());
        System.out.println("\nRectangle of red border");
        redRectangle.draw();
    }


}
