package com.lwl.designpattern.decorator;

/**
 * 用于返回数据包的核心内容,但是不负责将其构造成一个格式工整,可直接发布的数据格式
 * Created by liwenlong on 2018/1/19 16:52
 */
public class PacketBodyCreator implements PacketCreatorI {
    @Override
    public String handleContent() {
        return "content of packet";
    }
}
