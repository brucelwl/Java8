package com.lwl.designpattern.decorator;

/**
 * <pre>
 * 装饰接口,用于处理具体的内容
 *
 *
 * </pre>
 * Created by liwenlong on 2018/1/19 16:41
 */
public interface PacketCreatorI {
   public String handleContent();
}
