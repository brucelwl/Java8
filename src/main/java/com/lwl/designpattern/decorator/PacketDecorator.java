package com.lwl.designpattern.decorator;

/**PacketDecorator 维护核心组件component对象,负责告诉其子类,其核心业务逻辑
 * 应该委托component完成,自己仅仅做增强处理
 * Created by liwenlong on 2018/1/19 16:57
 */
public abstract class PacketDecorator implements PacketCreatorI {
    protected PacketCreatorI component;

    public PacketDecorator(PacketCreatorI component) {
        this.component = component;
    }
}
