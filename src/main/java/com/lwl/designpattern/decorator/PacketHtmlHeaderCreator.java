package com.lwl.designpattern.decorator;

/**
 * Created by liwenlong on 2018/1/19 17:01
 */
public class PacketHtmlHeaderCreator extends PacketDecorator{
    public PacketHtmlHeaderCreator(PacketCreatorI component) {
        super(component);
    }

    @Override
    public String handleContent() {
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<body>");
        sb.append(component.handleContent());
        sb.append("</html>");
        sb.append("</body>");
        return sb.toString();
    }
}
