package com.lwl.designpattern.decorator;

import java.time.LocalDateTime;

/**
 * Created by liwenlong on 2018/1/19 17:06
 */
public class PacketHttpHeaderCreator extends PacketDecorator {
    public PacketHttpHeaderCreator(PacketCreatorI component) {
        super(component);
    }

    @Override
    public String handleContent() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cache-Control:no-cache\n");
        sb.append("Date:").append(LocalDateTime.now()).append("\n");
        sb.append(component.handleContent());
        return sb.toString();
    }
}
