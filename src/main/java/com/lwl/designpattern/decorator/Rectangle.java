package com.lwl.designpattern.decorator;

/**
 * Created by liwenlong on 2018/1/19 17:58
 */
public class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("Shape: Rectangle");
    }
}
