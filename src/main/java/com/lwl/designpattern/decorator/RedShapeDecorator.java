package com.lwl.designpattern.decorator;

/**
 * 装饰器类
 * Created by liwenlong on 2018/1/21 14:27
 */
public class RedShapeDecorator extends ShapeDecorator {

    public RedShapeDecorator(Shape shape) {
        super(shape);
    }

    @Override
    public void draw() {
        decoratedShape.draw();
        System.out.println("Border Color: Red");
    }
}
