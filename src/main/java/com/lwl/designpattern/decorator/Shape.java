package com.lwl.designpattern.decorator;

/**
 * Created by liwenlong on 2018/1/19 17:57
 */
public interface Shape {
    void draw();
}
