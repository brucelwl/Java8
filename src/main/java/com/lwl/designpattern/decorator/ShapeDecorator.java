package com.lwl.designpattern.decorator;

/**
 * 抽象装饰类
 * Created by liwenlong on 2018/1/19 17:59
 */
public abstract class ShapeDecorator implements Shape{

    protected Shape decoratedShape;

    public ShapeDecorator(Shape shape) {
        this.decoratedShape = shape;
    }



}
