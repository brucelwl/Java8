package com.lwl.designpattern.flyweight;

/**
 * Created by liwenlong on 2018/1/18 14:51
 */
public class EmployeeReportManagerImpl implements ReportManagerI {
    private String tenantId = null;

    public EmployeeReportManagerImpl(String tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public String createReport() {
        return "this is a Employee report";
    }
}
