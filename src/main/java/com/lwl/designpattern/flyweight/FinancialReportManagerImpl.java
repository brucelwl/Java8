package com.lwl.designpattern.flyweight;

/**
 * 财务报表<br>
 * Created by liwenlong on 2018/1/18 14:50
 */
public class FinancialReportManagerImpl implements ReportManagerI {

    private String tenantId = null;

    public FinancialReportManagerImpl(String tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public String createReport() {
        return "this is a Financial report";
    }
}
