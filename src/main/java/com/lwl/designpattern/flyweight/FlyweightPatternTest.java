package com.lwl.designpattern.flyweight;

/**
 * <pre>O
 * 享元模式是设计模式中少数几个以提高系统性能为目的的模式之一
 * 享元模式对性能提升的主要帮助有两点
 * 1 可以节省重复创建对象的开销,因为被享元模式维护的相同对象只会被创建一次,当创建对象比较耗时,便可以节省大量时间
 * 2 由于创建对象的数量减少,所以对系统内存的需求也减少,gc压力降低,使得系统有一个更优化的内存结构和更快的反应速度
 *
 * 享元模式角色
 * 享元工厂: 用以创建具体的享元类,维护相同的享元对象.它保证相同的享元对象可以被系统
 * 共享.其内部使用了类似单例模式的算法,当请求对象已存在时,直接返回对象,不存在时再创建
 * 抽象享元: 定义需共享的对象的业务接口.
 * 具体享元类: 实现抽象享元类的接口,完成具体逻辑
 * </pre>
 * Created by liwenlong on 2018/1/18 14:52
 */
public class FlyweightPatternTest {

    public static void main(String[] args) {
        ReportManagerFactory factory = new ReportManagerFactory();
        ReportManagerI reportManager = factory.getEmployeeReportManager("A");
        System.out.println(reportManager.createReport());
    }



}
