package com.lwl.designpattern.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * 享元工厂类<br>
 * Created by liwenlong on 2018/1/18 18:16
 */
public class ReportManagerFactory {
    Map<String, ReportManagerI> financialReportManager = new HashMap<>();
    Map<String, ReportManagerI> employeeReportManager = new HashMap<>();

    public ReportManagerI getEmployeeReportManager(String tenantId) {
        ReportManagerI reportManagerI = employeeReportManager.get(tenantId);
        if (reportManagerI == null) {
            reportManagerI = new FinancialReportManagerImpl(tenantId);
            employeeReportManager.put(tenantId, reportManagerI);
        }
        return reportManagerI;
    }

    public ReportManagerI getFinancialReportManager(String tenantId) {
        ReportManagerI reportManagerI = financialReportManager.get(tenantId);
        if (reportManagerI == null) {
            reportManagerI = new EmployeeReportManagerImpl(tenantId);
            financialReportManager.put(tenantId, reportManagerI);
        }
        return reportManagerI;
    }
}
