package com.lwl.designpattern.flyweight;

/**
 * Created by liwenlong on 2018/1/18 14:48
 */
public interface ReportManagerI {

     String createReport();

}
