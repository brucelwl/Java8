package com.lwl.designpattern.observer;

/**
 * Created by liwenlong on 2018/1/21 17:40
 */
public class ConcreteObserver implements ObserverI {
    @Override
    public void update(final Event event) {
        System.out.println("observer receives information");
    }
}
