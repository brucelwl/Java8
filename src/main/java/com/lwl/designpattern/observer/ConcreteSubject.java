package com.lwl.designpattern.observer;

import java.util.Vector;

/**
 * <pre>
 *     实现一个具体的主题
 * </pre>
 * Created by liwenlong on 2018/1/21 15:50
 */
public class ConcreteSubject implements SubjectI {
    Vector<ObserverI> observers = new Vector<>();

    @Override
    public void attach(ObserverI observerI) {
        observers.addElement(observerI);
    }

    @Override
    public void detach(ObserverI observerI) {
        observers.removeElement(observerI);
    }

    @Override
    public void inform() {
        final Event event = new Event();
        observers.forEach(observerI -> observerI.update(event));
    }
}
