package com.lwl.designpattern.observer;

/**
 * <pre>
 *     观察者接口
 * </pre>
 * Created by liwenlong on 2018/1/21 15:39
 */
public interface ObserverI {
    void update(final Event event);
}
