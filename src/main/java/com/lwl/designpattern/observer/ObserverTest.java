package com.lwl.designpattern.observer;

import java.lang.ref.PhantomReference;
import java.nio.file.Files;
import java.util.*;

/**
 * <pre>
 *  观察者模式是非常常用的一种设计模式.在软件系统中,当一个对象的行为依赖另一个
 *  对象的状态时,观察者模式是相当有用的.
 *  在jdk中已经实现了一套观察者模式,可以直接通过继承java.util.Observable使用增加,删除和通知观察者
 * 当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新
 * 缺点： 1、如果一个被观察者对象有很多的直接和间接的观察者的话，将所有的观察者都通知到会花费很多时间。
 * 2、如果在观察者和观察目标之间有循环依赖的话，观察目标会触发它们之间进行循环调用，可能导致系统崩溃。
 * 3、观察者模式没有相应的机制让观察者知道所观察的目标对象是怎么发生变化的，而仅仅只是知道观察目标发生了变化。
 * 使用场景： 1、有多个子类共有的方法，且逻辑相同。 2、重要的、复杂的方法，可以考虑作为模板方法
 * </pre>
 * 
 * Created by liwenlong on 2018/1/21 15:23
 */
public class ObserverTest {

	public static void main(String[] args) {
		SubjectI subject = new ConcreteSubject();
		ObserverI observer = new ConcreteObserver();
		subject.attach(observer);
		subject.inform();

		long start = System.currentTimeMillis();
		String str = "aa,bb,cc";
		for (int j = 0; j < 10000; j++) {
			StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
			while (stringTokenizer.hasMoreElements()) {
				String nextToken = stringTokenizer.nextToken();
			}
		}
		System.out.println(System.currentTimeMillis() - start);

		start = System.currentTimeMillis();
		for (int j = 0; j < 10000; j++) {
			String[] split = str.split(",");
			for (int i = 0; i < split.length; i++) {
				String a = split[i];
			}
		}
		System.out.println(System.currentTimeMillis() - start);

		HashMap<String,String> map = new HashMap<>();
		map.put("","");

		LinkedHashMap<String,String> linkedHashMap = new LinkedHashMap<>(16);
		linkedHashMap.put("djfs","aaa");
		linkedHashMap.put("sdfyf","aaa");
		linkedHashMap.put("aaa","aaa");
		Set<Map.Entry<String, String>> entries = linkedHashMap.entrySet();
		for (Map.Entry<String, String> entry : linkedHashMap.entrySet()) {
			System.out.println(entry.getKey());
		}

		TreeMap<String,String> treeMap = new TreeMap<>();
		treeMap.subMap("","");
		treeMap.headMap("");
		treeMap.tailMap("");

		List<String> list = new ArrayList<>();
		LinkedList<String> objects = new LinkedList<>();
		if(objects instanceof RandomAccess){
			System.out.println(".........");
		}else{
			System.out.println("lllll");
		}

	}

}
