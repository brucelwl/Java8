package com.lwl.designpattern.observer;

/**
 * <pre>
 *     主题接口
 * </pre>
 * Created by liwenlong on 2018/1/21 15:38
 */
public interface SubjectI {

    /**
     * 添加观察者
     */
    void attach(ObserverI observerI);

    /**
     * 删除观察者
     */
    void detach(ObserverI observerI);

    /**
     * 通知所有的观察者
     */
    void inform();

}
