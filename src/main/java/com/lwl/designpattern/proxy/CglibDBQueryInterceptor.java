package com.lwl.designpattern.proxy;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * CgLib 代理 <br>
 * Created by liwenlong on 2018/1/18 13:57
 */
public class CglibDBQueryInterceptor implements MethodInterceptor {
    private DBQueryI dbQueryI;

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        if (dbQueryI == null) {
            dbQueryI = new DBQueryImpl();
        }
        return method.invoke(dbQueryI, objects);
    }
}
