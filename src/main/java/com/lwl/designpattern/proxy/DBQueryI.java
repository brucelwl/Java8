package com.lwl.designpattern.proxy;

/**
 * <pre>
 * 接口类
 *  代理模式的有点:
 *  1 可以实现对象的延时加载:如果当前并没有使用这个组件,则不需要真正的初始化
 *
 * </pre>
 *
 * Created by liwenlong on 2018/1/18 10:55
 */
public interface DBQueryI {
    String userName(String name);
    int userAge();
}
