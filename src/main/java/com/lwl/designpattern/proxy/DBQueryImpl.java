package com.lwl.designpattern.proxy;

/**
 * 接口实现类,真正实现代理的功能
 * Created by liwenlong on 2018/1/18 10:59
 */
public class DBQueryImpl implements DBQueryI {

    public DBQueryImpl(){
        System.out.println("DBQueryImpl create");
    }


    @Override
    public String userName(String name) {
        return "hello "+name;
    }

    @Override
    public int userAge() {
        return 15;
    }
}
