package com.lwl.designpattern.proxy;

/**
 * 代理类,调用实现类完成代理
 * Created by liwenlong on 2018/1/18 11:00
 */
public class DBQueryProxy implements DBQueryI {
    private DBQueryImpl impl = null;

    public DBQueryProxy() {
        System.out.println("DBQueryProxy create");
    }

    private DBQueryImpl getImpl() {
        if (impl == null) {
            impl = new DBQueryImpl();
        }
        return impl;
    }

    //只有真正调用方法时才初始化接口实现类
    @Override
    public String userName(String name) {
        return getImpl().userName(name);
    }

    @Override
    public int userAge() {
        return getImpl().userAge();
    }
}
