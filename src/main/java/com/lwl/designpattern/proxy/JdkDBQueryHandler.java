package com.lwl.designpattern.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * jdk 动态代理,动态代理可以减少代码,提升灵活性
 * Created by liwenlong on 2018/1/18 11:16
 */
public class JdkDBQueryHandler implements InvocationHandler {
    private DBQueryI dbQueryI = null;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        /*System.out.println("jdk invoke call");
        System.out.println(proxy.getClass().getName());
        System.out.println(method.getName());*/
        if (dbQueryI == null) {
            dbQueryI = new DBQueryImpl();
        }
        return method.invoke(dbQueryI, args);
    }
}
