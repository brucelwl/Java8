package com.lwl.designpattern.proxy;

import org.junit.Test;
import org.springframework.cglib.core.DebuggingClassWriter;
import org.springframework.cglib.proxy.Enhancer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 * Created by liwenlong on 2018/1/18 11:04
 */
public class ProxyTest {

    static {

        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "D:\\class"); //该设置用于输出cglib动态代理产生的类

    }

    @Test
    public void staticProxy() {

        DBQueryI queryI = new DBQueryProxy();

        System.out.println(queryI.userName("啊哈哈哈"));
        System.out.println(queryI.userAge());
    }

    @Test
    public void jdkProxy1() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        //ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        /*DBQueryI jdkProxy = (DBQueryI) Proxy.newProxyInstance(classLoader,
                new Class[]{DBQueryI.class}, new JdkDBQueryHandler());
        System.out.println(jdkProxy.userName("xiaoming"));
*/
        long start = System.currentTimeMillis();
        DBQueryI jdkProxy2 = (DBQueryI) Proxy.newProxyInstance(classLoader,
                new Class[]{DBQueryI.class}, new JdkDBQueryHandler());

        System.out.println("jdk 动态代理创建时间" + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            jdkProxy2.userName("xiaoming");
        }
        System.out.println("调用耗时" + (System.currentTimeMillis() - start));
    }

    public static void main(String[] args) {
        System.setProperty("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");   //该设置用于输出jdk动态代理产生的类
        ProxyTest proxyTest = new ProxyTest();
        proxyTest.jdkProxy1();
    }

    @Test
    public void jdkProxy2() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        long start = System.currentTimeMillis();

        Class<?> proxyClass = Proxy.getProxyClass(DBQueryI.class.getClassLoader(), DBQueryI.class);
        Constructor<?> constructor = proxyClass.getConstructor(InvocationHandler.class);
        DBQueryI dbQueryI = (DBQueryI) constructor.newInstance(new JdkDBQueryHandler());
        System.out.println("jdk 动态代理创建时间" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            dbQueryI.userName("xiaoming");
        }
        System.out.println("调用耗时" + (System.currentTimeMillis() - start));
    }

    @Test
    public void cglibProxy() {
        long start = System.currentTimeMillis();
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(new CglibDBQueryInterceptor());
        enhancer.setInterfaces(new Class[]{DBQueryI.class});

        //enhancer.setSuperclass(DBQueryImpl.class);
        DBQueryI dbQueryI = (DBQueryI) enhancer.create();
        System.out.println("cglib 动态代理创建时间" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            dbQueryI.userName("hahah");
        }
        System.out.println("调用耗时" + (System.currentTimeMillis() - start));
    }


}
