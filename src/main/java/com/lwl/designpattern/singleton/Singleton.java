package com.lwl.designpattern.singleton;

/**
 * <pre>
 * 饿汉式单利模式
 * 无论是否使用了单利,只要该类被使用都会产生实例
 * 例如调用了createString()方法,没有使用实例对象
 * 同样创建了单利实例对象,为了解决这个问题使用延迟加载机制
 * </pre>
 * Created by liwenlong on 2018/1/17 20:31
 */
public class Singleton {

    private Singleton() {
        System.out.println("Singleton is create");
    }

    private static Singleton instance = new Singleton();

    public static Singleton getInstance() {
        return instance;
    }

    /*public static void createString(){
        System.out.println("createString");
    }

    public static void main(String[] args) {
        Singleton.createString();

    }*/


}
