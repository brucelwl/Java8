package com.lwl.designpattern.singleton;

/**
 * Created by liwenlong on 2018/1/17 23:31
 */
public class SingletonTest {

    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        Thread[] threads = new Thread[5];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j < 100000; j++) {
                    //Singleton.getInstance();
                    //LazySingleton.getInstance();
                    StaticSingleton.getInstance();
                }
            });
            threads[i].start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(System.currentTimeMillis() - start);
    }



}
