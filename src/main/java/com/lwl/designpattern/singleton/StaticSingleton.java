package com.lwl.designpattern.singleton;

/**
 * 静态懒加载单例模式
 * Created by liwenlong on 2018/1/17 23:27
 */
public class StaticSingleton {

    private StaticSingleton() {
        System.out.println("StaticSingleton is create");
    }

    private static class SingletonHolder {
        private static StaticSingleton staticSingleton = new StaticSingleton();
    }

    public static StaticSingleton getInstance() {
        return SingletonHolder.staticSingleton;
    }


}
