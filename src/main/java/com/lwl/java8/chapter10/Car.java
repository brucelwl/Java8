package com.lwl.java8.chapter10;

import java.util.Optional;

/**
 * Created by liwenlong on 2017/12/21 11:05 <br/>
 */
public class Car {

    //车可能进行了保险,也可能没有保险
    private Optional<Insurance> insurance;

    public Optional<Insurance> getInsurance() {
        return insurance;
    }
}
