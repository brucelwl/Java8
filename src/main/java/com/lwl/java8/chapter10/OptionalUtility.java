package com.lwl.java8.chapter10;

import java.util.Optional;

/**
 * Created by liwenlong on 2017/12/21 12:48 <br/>
 */
public class OptionalUtility {

    /**
     * 将 String 转换为 Integer ，并返回一个 Optional 对象
     * @param numStr
     * @return
     */
    public static Optional<Integer> strToInteger(String numStr){
        try {
            return Optional.of(Integer.parseInt(numStr));
        }catch (NumberFormatException e){
            return Optional.empty();
        }

    }






}
