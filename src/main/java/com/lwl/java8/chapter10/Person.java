package com.lwl.java8.chapter10;

import java.util.Optional;

/**
 * Created by liwenlong on 2017/12/21 11:04 <br/>
 */

public class Person {

    private Optional<Car> car;

    public Optional<Car> getCar() {
        return car;
    }
}
