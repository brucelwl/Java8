package com.lwl.java8.chapter11;

import com.lwl.java8.chapter11.Discount.Code;

/**
 * 
 * 
 * @author lwl 2017年12月23日 上午12:32:29
 */
public class Quote {

	private String shopName;
	private double price;
	private Discount.Code discountCode;

	public Quote(String shopName, double price, Code discountCode) {
		this.shopName = shopName;
		this.price = price;
		this.discountCode = discountCode;
	}

	public static Quote parse(String s) {
		String[] split = s.split(":");
		String shopName = split[0];
		double price = Double.parseDouble(split[1]);
		Discount.Code discountCode = Discount.Code.valueOf(split[2]);
		return new Quote(shopName, price, discountCode);
	}

	public String getShopName() {
		return shopName;
	}

	public double getPrice() {
		return price;
	}

	public Discount.Code getDiscountCode() {
		return discountCode;
	}

	
	
	
}
