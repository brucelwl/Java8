package com.lwl.java8.chapter12;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by liwenlong on 2017/12/26 11:43 <br/>
 */
public class Chapter12Test {

    @Test
    public void dateTest() {
        Date date = new Date(114, 2, 18);
        System.out.println(date);
    }

    @Test
    public void localDateTest() {
        LocalDate localDate = LocalDate.of(2020, 3, 29);
        System.out.println(localDate);

        System.out.println("年:" + localDate.getYear());
        System.out.println("月:" + localDate.getMonth().getValue());
        System.out.println("日:" + localDate.getDayOfMonth());
        System.out.println("星期:" + localDate.getDayOfWeek() + " " + localDate.getDayOfWeek().getValue());
        System.out.println("一年中的第" + localDate.getDayOfYear() + "天");

        System.out.println("这个月有:" + localDate.lengthOfMonth() + "天");
        System.out.println("这一年有:" + localDate.lengthOfYear() + "天");
        System.out.println("闰年:" + localDate.isLeapYear());
        //获取当前年月日
        LocalDate now = LocalDate.now();
        System.out.println(now.getDayOfMonth());
        int year = now.get(ChronoField.YEAR);
        System.out.println(year);


    }


    @Test
    public void localTimeTest() {
        LocalTime localTime = LocalTime.of(12, 47, 45);

        System.out.println(localTime);
        System.out.println(localTime.getHour());
        System.out.println(localTime.getMinute());
        System.out.println(localTime.getSecond());

        LocalDate localDate = LocalDate.parse("2014-03-18");
        System.out.println(localDate);
        LocalTime localTime1 = LocalTime.parse("13:45:20");
        System.out.println(localTime1);

    }

    @Test
    public void localDateTimeTest() {

        // 2014-03-18T13:45:20
        LocalDateTime dt1 = LocalDateTime.of(2014, Month.MARCH, 18, 13, 45, 20);
        System.out.println(dt1);
        System.out.println(dt1.toLocalDate());
        System.out.println(dt1.toLocalTime());

        LocalDate date = LocalDate.of(2017, 12, 15);
        LocalTime time = LocalTime.of(12, 48, 59);
        LocalDateTime dt2 = LocalDateTime.of(date, time);
        System.out.println(dt2);

        LocalDateTime dt3 = date.atTime(13, 45, 20);
        LocalDateTime dt4 = date.atTime(time);
        System.out.println(dt3);

        LocalDateTime dt5 = time.atDate(date);
        System.out.println(dt5);

        //LocalDateTime parse = LocalDateTime.parse("2017-12-26");
        //System.out.println(parse);
    }

    @Test
    public void instantTest() {
        Instant instant = Instant.ofEpochSecond(3);
        System.out.println(instant);
    }

    @Test
    public void durationTest() {
        Duration between = Duration.between(LocalTime.now(), LocalTime.of(17, 43, 56));
        System.out.println(between);

        long seconds = between.getSeconds();
        System.out.println(seconds);
        System.out.println(between.toHours());

        //抛出异常DateTimeException
        //Duration between1 = Duration.between(LocalDate.now(), LocalDate.of(2017, 12, 27));
        //System.out.println(between1.toHours());

        Period tenDays = Period.between(LocalDate.of(2014, 3, 8),
                LocalDate.of(2014, 3, 18));
        System.out.println(tenDays);

        Duration threeMinutes = Duration.ofMinutes(3);
        System.out.println(threeMinutes.getSeconds());
        Duration threeMinutes2 = Duration.of(3, ChronoUnit.MINUTES);
        Period tenDays2 = Period.ofDays(10);
        Period threeWeeks = Period.ofWeeks(3);
        Period twoYearsSixMonthsOneDay = Period.of(2, 6, 1);
    }

    @Test
    public void localDateDemo1() {
        LocalDate date1 = LocalDate.of(2014, 3, 18);
        //以比较直观的方式操纵 LocalDate 的属性
        LocalDate date2 = date1.withYear(2011);
        LocalDate date3 = date2.withDayOfMonth(25);
        LocalDate date4 = date3.with(ChronoField.MONTH_OF_YEAR, 9);

        System.out.println(date1.withMonth(12));
        System.out.println(date1);
        System.out.println(date2);
        System.out.println(date3);
        System.out.println(date4);

    }

    @Test
    public void localDateDemo2() {
        //以相对方式修改 LocalDate 对象的属性
        LocalDate date1 = LocalDate.of(2014, 3, 18);
        LocalDate date2 = date1.plusWeeks(1);
        LocalDate date3 = date2.minusYears(3);
        LocalDate date4 = date3.plus(6, ChronoUnit.MONTHS);
        System.out.println(date1);
        System.out.println(date2);
        System.out.println(date3);
        System.out.println(date4);
    }

    @Test
    public void demo() {

        LocalDate date = LocalDate.of(2014, 3, 18);
        date = date.with(ChronoField.MONTH_OF_YEAR, 9);
        date = date.plusYears(2).minusDays(10);
        LocalDate localDate = date.withYear(2011);
        System.out.println(date);

    }

    @Test
    public void temporalAdjusterTest() {


    }

    @Test
    public void formatTest() {
        LocalDate date = LocalDate.of(2014, 3, 18);
        String s1 = date.format(DateTimeFormatter.BASIC_ISO_DATE);
        String s2 = date.format(DateTimeFormatter.ISO_LOCAL_DATE);

        System.out.println(s1);
        System.out.println(s2);

        LocalDate date1 = LocalDate.parse("20140318", DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate date2 = LocalDate.parse("2014-03-18", DateTimeFormatter.ISO_LOCAL_DATE);

        //按照特定的模式创建格式器
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate date11 = LocalDate.of(2016, 3, 18);
        String formattedDate = date11.format(formatter);
        System.out.println(formattedDate);

        LocalDate date22 = LocalDate.parse(formattedDate, formatter);
        System.out.println(date22);

        //创建一个本地化的 DateTimeFormatter
        DateTimeFormatter italianFormatter = DateTimeFormatter.ofPattern("d. MMMM yyyy", Locale.ITALIAN);
        String formattedDate2 = date.format(italianFormatter); // 18. marzo 2014
        System.out.println(formattedDate2);
        LocalDate date3 = LocalDate.parse(formattedDate2, italianFormatter);

    }

    @Test
    public void zoneIdTest(){
        ZoneId.getAvailableZoneIds().forEach(System.out::println);

        System.out.println(TimeZone.getDefault().getID());

        ZoneId romeZone = ZoneId.systemDefault();

        LocalDate date = LocalDate.of(2014, Month.MARCH, 18);
        ZonedDateTime zdt1 = date.atStartOfDay(romeZone);

        //通过 ZoneId ，你还可以将 LocalDateTime 转换为 Instant
        LocalDateTime dateTime = LocalDateTime.of(2014, Month.MARCH, 18, 13, 45);
        //Instant instantFromDateTime = dateTime.toInstant()
        //通过反向的方式得到 LocalDateTime 对象
        Instant instant = Instant.now();
        LocalDateTime timeFromInstant = LocalDateTime.ofInstant(instant, romeZone);
    }

}
