package com.lwl.java8.chapter3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.lwl.java8.entity.Apple;

public class Chapter3 {

	public void process(Runnable r) {
		r.run();
	}

	public Callable<String> fetch() {
		return () -> "Tricky example ;-)";
	}

	@Test
	public void lambdaTest() {
		Runnable r1 = () -> System.out.println("阿加速度哈斯的");
		Runnable r2 = new Runnable() {
			@Override
			public void run() {
				System.out.println("呵呵呵");
			}
		};
		process(r1);
		process(r2);
		process(() -> System.out.println("死咖啡色地方"));
	}

	public String processFile(BufferedReaderProcessor p) throws IOException {
		try (BufferedReader br = new BufferedReader(new BufferedReader(new FileReader("aa.txt")))) {
			return p.process(br);
		}
	}

	// @Test
	public void test2() {
		try {
			processFile((b) -> b.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Java 8 中引入了几个函数式接口Predicate,Consumer,Function
	@Test
	public void predicateTest() {
		List<String> list = new ArrayList<>();
		list.add("aaa");
		list.add("bbb");
		list.add("");
		list.add("ddd");
		System.err.println(JSON.toJSONString(list));
		
		char aa ='中';

		List<String> collect = list.parallelStream().filter((item) -> !item.isEmpty()).collect(Collectors.toList());
		System.err.println(JSON.toJSONString(collect));
	}

	@Test
	public void consumerTest() {
		List<String> list = new ArrayList<>();
		list.add("aaa");
		list.add("bbb");
		list.add("");
		list.add("ddd");
		list.forEach((item) -> System.out.println(item));
	}

	@Test
	public void functionTest() {
		List<String> list = new ArrayList<>();
		list.add("aaa");
		list.add("bbb");
		list.add("");
		list.add("ddd");

		List<String> collect = list.stream().map((item) -> item + "aa").collect(Collectors.toList());
		System.out.println(JSON.toJSONString(collect));
	}

	@Test
	public void lambdaArg() {
		int portNumber = 1337;

		Runnable r = () -> System.out.println(portNumber);
		r.run();
		// portNumber = 31337;
	}

	@Test
	public void methodTest() {
		List<String> list = Arrays.asList("a", "b", "A", "B");
		//list.sort(String::compareToIgnoreCase);
		//System.out.println(JSON.toJSONString(list));
		
		list.sort((a,b)->b.compareToIgnoreCase(a));
		System.out.println(JSON.toJSONString(list));
	}
	
	@Test
	public void constructorTest(){
		Supplier<Apple> c1 = ()->new Apple();
		Apple apple = c1.get();
		
		Supplier<Apple> c2 = Apple::new;
		Apple apple2 = c2.get();
		
		apple.setColor("red");
		apple.setWeight(150);
		System.out.println(JSON.toJSONString(apple));
		
		apple2.setColor("green");
		apple2.setWeight(150);
		System.out.println(JSON.toJSONString(apple2));
		
	}
	
	
	

}
