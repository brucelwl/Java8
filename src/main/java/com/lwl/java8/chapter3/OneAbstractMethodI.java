package com.lwl.java8.chapter3;

@FunctionalInterface
public interface OneAbstractMethodI {

	void runBack();

	default void sayHello(String name) {
		System.out.println("hello " + name);
	}

	@Override
	boolean equals(Object obj);

}
