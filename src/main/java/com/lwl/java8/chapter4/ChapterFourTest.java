package com.lwl.java8.chapter4;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class ChapterFourTest {

	List<Dish> menu;

	@Before
	public void before() {
		menu = Arrays.asList(new Dish("chicken", false, 800, Dish.Type.MEAT), new Dish("chicken", false, 700, Dish.Type.MEAT),
				new Dish("chicken", false, 400, Dish.Type.MEAT), new Dish("chicken", true, 530, Dish.Type.OTHER),
				new Dish("chicken", true, 350, Dish.Type.OTHER), new Dish("chicken", true, 120, Dish.Type.OTHER),
				new Dish("chicken", true, 550, Dish.Type.OTHER), new Dish("chicken", false, 300, Dish.Type.FISH),
				new Dish("chicken", false, 450, Dish.Type.FISH));
	}

	@Test
	public void test1() {

		List<String> names = menu.stream()
				.map(Dish::getName)
				.collect(Collectors.toList());

		System.out.println(JSON.toJSONString(names));
	}

	@Test
	public void test2() {
		List<String> title = Arrays.asList("Java8", "In", "Action");
		Stream<String> s = title.stream();
		s.forEach(System.out::println);
	}
	
	@Test
	public void test3() {
		//menu.stream().forEach(System.out::println);
		List<Dish> collect = menu.stream().limit(4)
				.filter(dish->dish.getCalories() < 700)
				.collect(Collectors.toList());
		System.out.println(JSON.toJSONString(collect));
	}
	
	@Test
	public void test4() {
	/*	List<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4);
		numbers.stream()
		   .filter(i -> i % 2 == 0)
		   .distinct()
		   .forEach(System.out::println);*/
		List<Dish> collect = menu.stream().filter(item->item.getCalories() > 300)
		    .distinct().collect(Collectors.toList());
		System.out.println(collect.size());
	}

}
