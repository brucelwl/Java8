package com.lwl.java8.chapter5;

public class Trader {
	private String name;
	private String city;

	public Trader(String n, String c) {
		this.name = n;
		this.city = c;
	}

	public String getName() {
		return this.name;
	}

	public String getCity() {
		return this.city;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Trader){
			Trader trader =  (Trader)obj;
			return trader.getCity().equals(this.getCity()) 
					&& trader.getName().equals(this.getName());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		//int hashCode = super.hashCode();
		int hashCode = (this.name + this.city).hashCode();
		return hashCode;
	}

	public String toString() {
		//System.out.println("toString");
		return "Trader:" + this.name + " in " + this.city;
	}
}
