package com.lwl.java8.chapter6;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.lwl.java8.chapter4.Dish;
import com.lwl.java8.chapter4.Dish.Type;
import com.lwl.java8.entity.Data;

public class Chapter6Test {

	@Test
	public void test1() {
		IntSummaryStatistics summary = Data.menus().stream().collect(Collectors.summarizingInt(Dish::getCalories));
		System.out.println(JSON.toJSONString(summary));

		String collect = Data.menus().stream().map(Dish::getName).collect(Collectors.joining());
		System.out.println(collect);
		

		String collect2 = Data.menus().stream().map(Dish::getName).collect(Collectors.joining(", "));
		System.out.println(collect2);
		
	}
	
	@Test
	public void test2() {
		Map<Type, List<Dish>> group = Data.menus().stream().collect(Collectors.groupingBy(Dish::getType));
		System.out.println(JSON.toJSONString(group));
		
	}
	
	
	
	
	

}
