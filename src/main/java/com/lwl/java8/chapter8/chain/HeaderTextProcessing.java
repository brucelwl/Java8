package com.lwl.java8.chapter8.chain;

public class HeaderTextProcessing extends ProcessingObject<String>{

	@Override
	protected String handleWork(String text) {
		return "From Raoul, Mario and Alan: " + text;
	}

	
}
