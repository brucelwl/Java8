package com.lwl.java8.chapter8.chain;

/**
 * 责任链模式
 * @param <T>
 * @author lwl  2017年12月16日 下午11:39:26
 */
public abstract class ProcessingObject<T> {
	
	protected ProcessingObject<T> doThen;
	public void doThen(ProcessingObject<T> then){
		this.doThen = then;
	}
	
	public T handle(T input){
		T r = handleWork(input);
		if(doThen != null){
			return doThen.handle(r);
		}
		return r;
	}
	
	abstract protected T handleWork(T input);
}

