package com.lwl.java8.chapter8.chain;

public class SpellCheckerProcessing extends ProcessingObject<String> {

	@Override
	protected String handleWork(String text) {
		return text.replaceAll("labda", "lambda");
	}

}
