
package com.lwl.java8.chapter8.observer;

import java.util.ArrayList;
import java.util.List;

public class Feed implements Subject {
	
	private final List<Observer> observers = new ArrayList<>();

	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void notifyObservers(String subject) {
		observers.forEach(o->o.notify(subject));
	}
}
