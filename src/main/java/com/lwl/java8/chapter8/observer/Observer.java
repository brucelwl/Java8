package com.lwl.java8.chapter8.observer;

/**
 * 观察者模式
 * @author lwl  2017年12月16日 下午10:14:55
 */
@FunctionalInterface
public interface Observer {
	
	void notify(String tweet);

}
