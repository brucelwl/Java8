package com.lwl.java8.chapter8.observer;

public interface Subject {
	void registerObserver(Observer o);
	void notifyObservers(String tweet);
}
