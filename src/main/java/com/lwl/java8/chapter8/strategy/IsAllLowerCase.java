package com.lwl.java8.chapter8.strategy;

/**
 * 策略模式实现类,验证
 * 
 * @author lwl  2017年12月16日 下午8:30:03
 */
public class IsAllLowerCase implements ValidationStrategy{

	@Override
	public boolean execute(String str) {
		return str.matches("[a-z]+");
	}

}

