package com.lwl.java8.chapter8.strategy;

public class IsNumeric implements ValidationStrategy{

	@Override
	public boolean execute(String strNum) {
		return strNum.matches("\\d+");
	}

}
