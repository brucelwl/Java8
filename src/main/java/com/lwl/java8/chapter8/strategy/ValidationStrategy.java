package com.lwl.java8.chapter8.strategy;

/**
 * 策略模式
 * 
 * @author lwl  2017年12月16日 下午8:29:14
 */
public interface ValidationStrategy {
	
	boolean execute(String s);
	
	

}
