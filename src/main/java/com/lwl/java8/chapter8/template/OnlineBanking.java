package com.lwl.java8.chapter8.template;

import java.util.function.Consumer;

/**
 * 模板方法抽象类
 * @author lwl  2017年12月16日 下午9:44:51
 */
public abstract class OnlineBanking {
	
	/**
	 * 模拟数据源
	 */
	private Customer getCustomer(int id){
		Customer customer = null;
		switch (id) {
		case 1:
			customer = new Customer("aa",1);
			break;
		case 2:
			customer = new Customer("bb",2);
			break;
		case 3:
			customer = new Customer("cc",3);
			break;
		default:
			break;
		}
		return customer;
	}
	
	protected abstract void makeCustomerHappy(Customer customer);

	public void processCustomer(int id) {
		Customer customer = getCustomer(id);
		makeCustomerHappy(customer);
	}
	
	public void processCustomer(int id,Consumer<Customer> makeCustomerHappy){
		Customer customer = getCustomer(id);
		makeCustomerHappy.accept(customer);
	}
	
	
}

