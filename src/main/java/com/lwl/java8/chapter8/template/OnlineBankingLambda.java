package com.lwl.java8.chapter8.template;

public class OnlineBankingLambda extends OnlineBanking{

	@Override
	public void makeCustomerHappy(Customer customer) {
		System.out.println("通过继承的方式执行公用业务逻辑外的逻辑"+customer.toString());
	}


}
