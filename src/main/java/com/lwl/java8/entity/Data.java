package com.lwl.java8.entity;

import java.util.Arrays;
import java.util.List;

import com.lwl.java8.chapter4.Dish;
import com.lwl.java8.chapter5.Trader;
import com.lwl.java8.chapter5.Transaction;

public abstract class Data {

	public static List<Transaction> transactions() {

		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader raoul2 = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");

		List<Transaction> transactions = Arrays.asList(new Transaction(brian, 2011, 300),
				new Transaction(raoul2, 2012, 1000), new Transaction(raoul, 2011, 400),
				new Transaction(mario, 2012, 710), new Transaction(mario, 2012, 700), new Transaction(alan, 2012, 950));
		return transactions;
	}

	public static List<Dish> menus() {
		List<Dish> menu;
		menu = Arrays.asList(new Dish("chicken1", false, 800, Dish.Type.MEAT),
				new Dish("chicken2", false, 700, Dish.Type.MEAT), new Dish("chicken", false, 400, Dish.Type.MEAT),
				new Dish("chicken3", true, 530, Dish.Type.OTHER), new Dish("chicken", true, 350, Dish.Type.OTHER),
				new Dish("chicken4", true, 120, Dish.Type.OTHER), new Dish("chicken", true, 550, Dish.Type.OTHER),
				new Dish("chicken5", false, 300, Dish.Type.FISH), new Dish("chicken", false, 450, Dish.Type.FISH));
		return menu;
	}

}
