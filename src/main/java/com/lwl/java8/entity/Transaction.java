package com.lwl.java8.entity;

public class Transaction {
	
	private int price;
	
	private Currency currency;
	
	public Transaction() {
	}

	public Transaction(int price) {
		this.price = price;
	}

	public Transaction(int price, Currency currency) {
		this.price = price;
		this.currency = currency;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	
}
