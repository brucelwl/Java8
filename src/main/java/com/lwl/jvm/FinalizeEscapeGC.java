package com.lwl.jvm;

public class FinalizeEscapeGC {
	
	public static FinalizeEscapeGC save_hook = null;
	
	public void isAlive(){
		System.out.println("yes,i am still alive:");
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		System.out.println("finalize method executed");
		FinalizeEscapeGC.save_hook = this;
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		save_hook = new FinalizeEscapeGC();
		//对象第一次成功拯救自己
		save_hook = null;
		System.gc();
		Thread.sleep(500);
		if(save_hook != null){
			save_hook.isAlive();
		}else{
			System.out.println("no,i am dead");
		}
		
		save_hook = null;
		System.gc();
		Thread.sleep(500);
		if(save_hook != null){
			save_hook.isAlive();
		}else{
			System.out.println("no i am dead...");
		}
	}

}
