package com.lwl.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * java堆溢出<br/>
 * java堆用于存储对象实例<br/>
 * 将堆的最小值-Xms参数和最大值参数-Xmx参数设置为一样可以避免堆自动扩展<br>
 * 通过参数-XX:+HeapDumpOnOutOfMemoryError可以让虚拟机在出现内存溢出时dump出内存堆转储快照<br>
 * -Xms20m -Xmx20m -XX:+HeapDumpOnOutOfMemoryError
 * @author lwl  2017年12月20日 下午11:57:44
 */
public class HeapOOM {
	
	static class OOMObject{
		
	}
	
	public static void main(String[] args) {
		List<OOMObject> list = new ArrayList<>();
		while(true){
			list.add(new OOMObject());
		}
	}
}
