package com.lwl.jvm;

/**
 * -Xss2M
 * 
 * @author lwl  2017年12月21日 上午12:42:10
 */
public class JavaVMStackOOM {
	
	private void dontStop(){
		while(true){
			
		}
	}
	
	public void stackLeakByThread(){
		while(true){
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					dontStop();
				}
			});
			thread.start();
		}
	}

	public static void main(String[] args) {
		//JavaVMStackOOM oom = new JavaVMStackOOM();
		//oom.stackLeakByThread();
	}
	
}
