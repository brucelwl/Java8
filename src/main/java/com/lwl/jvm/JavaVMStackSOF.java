package com.lwl.jvm;

/**
 * java虚拟机栈和本地方法栈溢出<br>
 * 使用 -Xss参数减少栈内存容量<br>
 * -Xss128K
 * @author lwl  2017年12月21日 上午12:23:17
 */
public class JavaVMStackSOF {
	private int stackLength = 1;
	public void stackLeak(){
		stackLength++;
		stackLeak();
	}

	public static void main(String[] args) {
		JavaVMStackSOF oom = new JavaVMStackSOF();
		try {
			oom.stackLeak();
		} catch (Throwable e) {
			System.out.println("stack length:"+oom.stackLength);
			throw e;
		}
	}
}
