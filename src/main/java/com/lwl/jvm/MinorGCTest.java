package com.lwl.jvm;

/**
 * <pre>
 * 对象在堆内存分配
 * Java堆(分代)
 *      -> 新生代(Eden:survivor = 8:1 默认)
 *             -->Eden区
 *             -->两个survivor区
 *       -> 老年代
 * 堆的最小值-Xms参数和最大值参数-Xmx<br> -Xmn10M设置新生代堆为10M剩下的给老年代
 * -XX:SurvivorRatio 设置新生代的Eden:survivor为 8:1
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8</pre>
 * @author lwl  2017年12月24日 下午3:17:13
 */
public class MinorGCTest {
	
	private static final int _1MB = 1024 * 1024;
	public static void main(String[] args) {
		
		MinorGCTest minorGC = new MinorGCTest();
		minorGC.testAllocation();
	}
	
	@SuppressWarnings("unused")
	private void testAllocation() {
		byte[] allocation1,allocation2,allocation3;
		allocation1 = new byte[2 * _1MB];
		allocation2 = new byte[2 * _1MB];
		allocation3 = new byte[2 * _1MB];
		
		System.gc();
		//minor GC
		allocation3 = new byte[4 * _1MB];
		
		try {
			Thread.sleep(50000000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	

}
