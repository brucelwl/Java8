package com.lwl.jvm;

public class ReferenceCountingGC {
	public Object instance = null;
	private static final int _1MB = 1024 * 1024;
	private byte[] bigSize = new byte[20*_1MB];
	
	public static void main(String[] args) throws InterruptedException {
		ReferenceCountingGC objA = new ReferenceCountingGC();
		ReferenceCountingGC objB = new ReferenceCountingGC();
		objA.instance = objB;
		objB.instance = objA;
		objA = null;
		objB = null;
		
		System.gc();
		Thread.sleep(5000);
	}

}
