package com.lwl.jvm;

/**
 * java7及以后的虚拟机String.intern()方法实现不再是复制实例
 * 而是在常量池记录首次出现的实例引用
 * 
 * @author lwl  2017年12月20日 下午11:24:31
 */
public class RuntimeConstantPoolOOM {
	public static void main(String[] args) {
		String str1 = new StringBuilder("计算机").append("软件").toString();
		System.out.println(str1.intern() == str1);
		
		//java已经在常量池出现,估计是虚拟机启动后加载到常量池,所以返回false
		String str2 = new StringBuilder("ja").append("va").toString();
		System.out.println(str2.intern() == str2);
		
		
	}
	

}
