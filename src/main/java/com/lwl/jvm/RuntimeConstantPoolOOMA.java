package com.lwl.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * 方法区和堆一样是各个线程共享的内存区域<br/>
 * 方法区用于存放Class的相关信息,如类名,访问修饰符,常量池,字段描述,方法描述等<br/>
 * 虚拟机规范把方法区描述为堆的一个逻辑部分,但有个别名叫Non-Heap(非堆)<br/>
 * 运行时常量池是方法区的一部分<br/>
 * 限制方法区大小<br/>
 * vm args: -XX:PermSize=10M -XX:MaxPermSize=10M<br/>
 * ignoring option PermSize=10M; support was removed in 8.0<br/>
 * ignoring option MaxPermSize=10M; support was removed in 8.0
 * @author lwl  2017年12月20日 下午11:27:27
 */
public class RuntimeConstantPoolOOMA {
	public static void main(String[] args) {
		//使用List保持着常量池引用,避免Full GC回收常量池行为
		List<String> list = new ArrayList<>();
		int i = 0;
		while(true){
			//intern() java7以后再方法区的常量池中存放实例引用,而不是实例
			list.add(String.valueOf(i++).intern());
		}
	}

}
