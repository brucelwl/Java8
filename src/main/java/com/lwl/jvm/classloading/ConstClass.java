package com.lwl.jvm.classloading;

/**
 * <pre>
 * 被动使用类字段演示三
 * 常量在编译阶段会出入调用类的常量池中,本质上并没有直接引用定义常量的类
 * 因此不会触发定义常量的类的初始化
 * <pre>
 * @author lwl  2017年12月24日 下午11:51:23
 */
public class ConstClass {
	
	public static final String helloWorld = "hello world";
	static{
		System.out.println("ConstClass init");
	}
	
	
	

}
