package com.lwl.jvm.classloading;

import java.lang.reflect.Method;

/**
 * @author liwenlong - 2018/5/27 14:33
 */
public class MyClassLoaderTest {

    public static void main(String[] args) throws Exception {

        MyClassLoader myClassLoader = new MyClassLoader("E:\\mylib");

        Class<?> aClass = myClassLoader.loadClass("com.lwl.jvm.classloading.CustomLoaderClass");

        if (aClass != null){
            Object obj=aClass.newInstance();
            Method method=aClass.getMethod("hello", null);
            method.invoke(obj, null);
            System.out.println(aClass.getClassLoader().toString());
        }

    }









}
