package com.lwl.jvm.classloading;

public class NotInit {
	
	/*
	 * 静态语句块只能访问到定义在静态语句块的之前的变量
	 */
	static{
		i = 0;
		//System.out.println("main class init"+i);
	}
	
	static int i = 1;
	
	
	public static void main(String[] args) {
		//通过子类引用父类的静态字段,不会导致类初始化
		//System.out.println(SubClass.value);
		
		//通过数组定义来引用类,不会触发此类的初始化
		//SupperClass[] sca = new SupperClass[2];
		//List<SupperClass> list = new ArrayList<>();
		
		//不会触发定义常量的类的初始化
		System.out.println(ConstClass.helloWorld);
		
	}
	
	

}
