package com.lwl.jvm.thread;

/**
 * Created by liwenlong on 2017/12/25 11:50 <br/>
 */
public class VolatileTest {

    public static volatile int race = 0;
    private static final int thread_count = 20;

    //使用synchronized锁静态方法可得到正确计算结果
    //public synchronized static void increase(){
    public static void increase(){
        race++;
    }

    public static void main(String[] args) {
        Thread[] threads = new Thread[thread_count];
        for (int i = 0; i < thread_count; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 10000; j++) {
                        increase();
                    }
                }
            });
            threads[i].start();
        }
        while (Thread.activeCount() > 1){
            Thread.yield();
        }
        System.out.println("累加结果:"+race);
    }







}
