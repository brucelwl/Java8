package com.lwl.nio;

import java.nio.CharBuffer;
import java.util.Iterator;

public class BufferFillDrain {

	private static int index = 0;
	private static String[] strs = { "A random string value", "The product of an infinite number of monkeys",
			"Hey hey we're the Monkees", "Opening act for the Monkees: Jimi Hendrix", "'Scuse me while I kiss this fly", // Sorry
																															// Jimi
																															// ;-)
			"Help Me! Help Me!", };

	private static boolean fillBuffer(CharBuffer buffer) {
		if (index >= strs.length) {
			return false;
		}
		String str = strs[index++];
		for (int i = 0; i < strs.length; i++) {
			buffer.put(str.charAt(i));
		}
		return true;
	}

	private static void drainBuffer(CharBuffer buffer) {
		while (buffer.hasRemaining()) {
			System.out.print(buffer.get());
		}
		System.out.println("");
	}

	public static void main(String[] args) {

		CharBuffer charBuffer = CharBuffer.allocate(100);

		while (fillBuffer(charBuffer)) {
			charBuffer.flip();
			drainBuffer(charBuffer);
			charBuffer.clear();
		}

	}

}
