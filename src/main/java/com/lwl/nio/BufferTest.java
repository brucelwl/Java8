package com.lwl.nio;

import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

import org.junit.Test;

public class BufferTest {

	@Test
	public void test1() {
		ByteBuffer byteBuffer = ByteBuffer.allocate(10);
		byteBuffer.put((byte) 'h').put((byte) 'e').put((byte) 'l').put((byte) 'l').put((byte) 'o');

		System.out.println((char) byteBuffer.get());

		byteBuffer.limit(byteBuffer.position()).position(0);

		System.out.println((char) byteBuffer.get());

		byteBuffer.put(0, (byte) 'M');

		System.out.println((char) byteBuffer.get());

		byteBuffer.flip();
		System.out.println((char) byteBuffer.get());

		byteBuffer.rewind();
		System.out.println((char) byteBuffer.get());
	}
	
	@Test
	public void test2() {
		
		String str = "hello";
		ByteBuffer byteBuffer = ByteBuffer.allocate(10);
		byteBuffer.put(str.getBytes());
		//ByteBuffer byteBuffer = ByteBuffer.wrap(str.getBytes());
		byte[] data = new byte[10];
		for (int i = 0;byteBuffer.hasRemaining(); i++) {
			data[i] = byteBuffer.get();
		}
		//System.out.println(byteBuffer.get());
		
		byteBuffer.flip();
		System.out.println(byteBuffer.get());
		
		//byteBuffer.compact();
		
		byteBuffer.clear();
		
	}
	
	
	
	

}
