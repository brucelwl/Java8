package com.lwl.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public class ChannelCopy {
	
	public static void main(String[] args) {
		
		
		
		
		
	}
	
	private static void channelCopy1(ReadableByteChannel src,WritableByteChannel dest) throws IOException {
		ByteBuffer directByteBuffer = ByteBuffer.allocateDirect(16 * 1024);
		
		while (src.read(directByteBuffer) != -1) {
			directByteBuffer.flip();
			dest.write(directByteBuffer);
			directByteBuffer.compact();
		}
		directByteBuffer.flip();
		while (directByteBuffer.hasRemaining()) {
			dest.write(directByteBuffer);
		}
	}
	
	private static void channelCopy2(ReadableByteChannel src,WritableByteChannel dest) throws IOException {
		ByteBuffer directByteBuffer = ByteBuffer.allocateDirect(16 * 1024);
		
		while (src.read(directByteBuffer) != -1) {
			directByteBuffer.flip();
			while (directByteBuffer.hasRemaining()) {
				dest.write(directByteBuffer);
			}
			directByteBuffer.clear();
		}
		
	}
	
	
	
	

}
