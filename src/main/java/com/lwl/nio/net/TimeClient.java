package com.lwl.nio.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by liwenlong on 2017/11/29 19:03
 */
public class TimeClient {

    private static final int default_port = 37;
    private static final long diff_1900 = 2208988800L;
    protected int port = default_port;
    protected List<InetSocketAddress> remoteHosts;
    protected DatagramChannel channel;


    public TimeClient() throws IOException {
        parseArgs ();
        this.channel = DatagramChannel.open();
    }

    protected InetSocketAddress receivePacket(DatagramChannel channel, ByteBuffer byteBuffer) throws IOException {
        byteBuffer.clear();
        return (InetSocketAddress) channel.receive(byteBuffer);
    }


    private void sendRequests() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1);
        Iterator<InetSocketAddress> iterator = remoteHosts.iterator();
        while (iterator.hasNext()) {
            InetSocketAddress address = iterator.next();
            System.out.println("requesting time from " + address.getHostName() + ":" + address.getPort());
            buffer.clear().flip();
            channel.send(buffer, address);
        }
    }

    //Receive any replies that arrive
    private void getReplies() throws IOException {
        ByteBuffer longBuffer = ByteBuffer.allocate(8);
        longBuffer.order(ByteOrder.BIG_ENDIAN);
        longBuffer.putLong(0, 0);
        longBuffer.position(4);
        ByteBuffer buffer = longBuffer.slice();
        int expect = remoteHosts.size();
        int replies = 0;
        System.out.println();
        System.out.println("Waiting for replies...");
        while (true) {
            InetSocketAddress address = receivePacket(channel, buffer);
            buffer.flip();
            replies++;
            printTime(longBuffer.getLong(0), address);
            if (replies == expect) {
                System.out.println("All packets answered");
                break;
            }
            // Some replies haven't shown up yet
            System.out.println("Received " + replies
                    + " of " + expect + " replies");
        }
    }

    private void printTime(long remote1900, InetSocketAddress address) {

        long local = System.currentTimeMillis() / 1000;
        long remote = remote1900 - diff_1900;
        Date remoteDate = new Date(remote * 1000);
        Date localDate = new Date(local * 1000);
        long skew = remote - local;
        System.out.println("Reply from " + address.getHostName() + ":" + address.getPort());
        System.out.println(" there: " + remoteDate);
        System.out.println(" here: " + localDate);
        System.out.print(" skew: ");
        if (skew == 0) {
            System.out.println("none");
        } else if (skew > 0) {
            System.out.println(skew + " seconds ahead");
        } else {
            System.out.println((-skew) + " seconds behind");
        }
    }

    private void parseArgs() {
        String[] argv = new String[]{"1111","22222","33333"};
        remoteHosts = new LinkedList<>();
        for (int i = 0; i < argv.length; i++) {
            String arg = argv[i];
            // Send client requests to the given port
            if (arg.equals("-p")) {
                i++;
                this.port = Integer.parseInt(argv[i]);
                continue;
            }
            // Create an address object for the hostname
            InetSocketAddress sa = new InetSocketAddress(arg, port);
            // Validate that it has an address
            if (sa.getAddress() == null) {
                System.out.println("Cannot resolve address: "
                        + arg);
                continue;
            }
            remoteHosts.add(sa);
        }
    }

    public static void main(String[] argv)
            throws Exception {
        TimeClient client = new TimeClient();
        client.sendRequests();
        client.getReplies();
    }

}
