package com.lwl.nio.reg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;

public class RegexMatches {
	
	public static void main(String[] args) {
		// 按指定模式在字符串查找
	    String line = "This order was placed for QT3000! OK?";
		String reg = "(.*)(\\d+)(.*)";
		//创建Pattern对象
		Pattern pattern = Pattern.compile(reg);
		//创建 matcher 对象
		Matcher matcher = pattern.matcher(line);
		if(matcher.find()){
		   	System.out.println("find value: "+matcher.group(0));
		   	System.out.println("find value: "+matcher.group(1));
		   	System.out.println("find value: "+matcher.group(2));
		   	System.out.println(JSON.toJSONString(matcher));
		}else{
			System.out.println("no match");
		}
		
	}
	
	
	
	
	

}
