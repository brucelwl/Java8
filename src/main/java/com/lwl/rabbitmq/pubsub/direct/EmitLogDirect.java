package com.lwl.rabbitmq.pubsub.direct;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class EmitLogDirect {

	private static final String EXCHANGE_NAME = "direct_logs";

	public static void main(String[] args) throws IOException, TimeoutException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("127.0.0.1");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);

		String[] routingKeys = new String[] { "info", "warning", "error" };
		Random random = new Random();
		for (int i = 0; i < 20; i++) {
			int key = random.nextInt(1000) % 3;
			channel.basicPublish(EXCHANGE_NAME, routingKeys[key], null, (routingKeys[key] + " message"+i).getBytes());
			System.out.println((routingKeys[key] + " message"));
		}
		channel.close();
		connection.close();
	}

}
