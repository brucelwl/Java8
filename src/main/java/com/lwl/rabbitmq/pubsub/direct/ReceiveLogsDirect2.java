package com.lwl.rabbitmq.pubsub.direct;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class ReceiveLogsDirect2 {
	private static final String exchange = "direct_logs";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("127.0.0.1");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		// 创建订阅模式
		channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT);
		// 随机生成队列的名字
		String queueName = channel.queueDeclare().getQueue();
		// 需要绑定的routingKey
		String routingKey = "error";
		channel.queueBind(queueName, exchange, routingKey);

		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
				System.out.println(consumerTag + " " + JSON.toJSONString(properties));
			}
		};
		channel.basicConsume(queueName, true, consumer);
	}

}
