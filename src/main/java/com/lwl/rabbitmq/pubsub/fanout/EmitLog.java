package com.lwl.rabbitmq.pubsub.fanout;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class EmitLog {

	private static final String exchange = "1ogs";
	private static final String host = "127.0.0.1";
	
	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT);
		
		for (int i = 0; i < 100; i++) {
			String message = ("hello pub sub"+i);
			channel.basicPublish(exchange, "", null, message.getBytes() );
			System.out.println(" [x] Sent '" + message + "'");
		}
		channel.close();
		connection.close();
	}
	
	
	
	
	
	
}
