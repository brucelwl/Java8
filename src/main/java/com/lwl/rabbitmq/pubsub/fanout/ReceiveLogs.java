package com.lwl.rabbitmq.pubsub.fanout;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;

public class ReceiveLogs {
	private static final String exchange = "1ogs";
	
	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("127.0.0.1");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT);
		String queueName = channel.queueDeclare().getQueue();
		System.out.println("queueName..."+queueName);
		channel.queueBind(queueName, exchange, "");
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		DefaultConsumer defaultConsumer = new DefaultConsumer(channel){
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body)
					throws IOException {
				String message = new String(body);
				System.out.println(" [x] Received '" + message + "'");
			}
		};
		channel.basicConsume(queueName, true,defaultConsumer);
	}
	
	
	
	
	

}
