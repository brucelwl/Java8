package com.lwl.rabbitmq.simple;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * Created by liwenlong on 2017/11/25 16:19
 */
public class Recv {

    private final static String queue_name = "hello";
    private final static String host = "127.0.0.1";


    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(queue_name,false,false,false,null);

        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {

                String message = new String(body, StandardCharsets.UTF_8);

                System.out.println("received " + message);

            }
        };

        channel.basicConsume(queue_name,true,defaultConsumer);

        System.out.println("[*] Waiting for message. To exit press ctrl+c");
    }




}
