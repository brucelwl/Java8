package com.lwl.rabbitmq.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * Created by liwenlong on 2017/11/25 14:34
 */
public class Send {


    private final static String queue_name = "hello";
    private final static String host = "127.0.0.1";


    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(queue_name,true,false,false,null);
        String message = "Hello World 中文";
        channel.basicPublish("",queue_name,null,message.getBytes(StandardCharsets.UTF_8));

        System.out.println("sent "+message);

        channel.close();
        connection.close();


    }


}
