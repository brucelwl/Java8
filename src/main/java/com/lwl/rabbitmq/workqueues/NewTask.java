package com.lwl.rabbitmq.workqueues;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class NewTask {

	private final static String queue_name = "WorkQueues";
	private final static String host = "127.0.0.1";

	public static void main(String[] args) throws IOException, TimeoutException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		boolean durable = true;//队列持久化
		channel.queueDeclare(queue_name, durable, false, false, null);
		String message = "Hello World 中文.";
		
		System.out.println(JSON.toJSONString(channel));
		
		for (int i = 0; i < 50; i++) {
			//MessageProperties.PERSISTENT_TEXT_PLAIN消息持久化
			channel.basicPublish("", queue_name, MessageProperties.PERSISTENT_TEXT_PLAIN, (message+i).getBytes(StandardCharsets.UTF_8));
			System.out.println("sent " + message);
		}
		channel.close();
		connection.close();

	}

	
}
