package test;

import com.alibaba.fastjson.JSON;
import com.lwl.utils.HttpUtilslwl;
import org.junit.Test;
import test.entity.TokenInfo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by liwenlong on 2017/12/1 12:44
 */
public class HttpUtilTest {

    //获取微信模板列表
    @Test
    public void getWechatTemplateList() {
        String token = "5_xaAMVC57AwDfER7P6bPcVGpjUd5Y7XjAsidQoa8A8gSHKHcrp-kNIjfI4qLP_sXIXlXnDDzY9spzd5eeOxSn1RnBhQlzTlEZDkqEfZOAVItXM-Vk3-8XQQlDEmKgJKF33GfIxeAzwpZAaKMtAHIgAJAWDE";
        String url = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=" + token;

        HttpUtilslwl httpUtilslwl = new HttpUtilslwl();
        HttpUtilslwl.HttpResp httpResp = httpUtilslwl.httpClientdoGet(url, null, null);
        System.out.println(httpResp.getBody());
    }

    @Test
    public void test1() {
        String url = "http://yuntuapi.amap.com/datasearch/local";
        Map<String,String> params = new HashMap<>();
        params.put("key","e2f238148228f1a48db7bb07d63933bd");
        params.put("tableid","58ea59dc305a2a06c43a566f");
        params.put("keywords","");
        params.put("city","全国");
        //params.put("filter","");
        //params.put("sortrule","");
        params.put("limit", "100");//分页数据条目数(每页数据展现的条数)
        //params.put("page","1"); //分页索引,当前页数
        params.put("sig",""); //数字签名

        HttpUtilslwl httpUtilslwl = new HttpUtilslwl();
        for (int i = 0; i < 2018; i++) {
            params.put("page", String.valueOf(i + 1)); //分页索引,当前页数
            HttpUtilslwl.HttpResp httpResp = httpUtilslwl.httpClientdoGet(url, params, null);

            //System.out.println(httpResp.getBody());

            LocationResp locationResp = JSON.parseObject(httpResp.getBody(), LocationResp.class);
            System.out.println(JSON.toJSONString(locationResp));
        }

    }

    @Test
    public void test2() {
        WeakHashMap<String, String> map = new WeakHashMap<>();
        map.put(new String("1"), "1");
        map.put("2", "2");
        String s = new String("3");
        map.put(s, "3");
        while (map.size() > 0) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ignored) {
            }
            System.out.println("Map Size:" + map.size());
            System.out.println(map.get("1"));
            System.out.println(map.get("2"));
            System.out.println(map.get("3"));
            System.out.println();
            System.gc();
        }
    }

    @Test
    public void nettyHttpRestTest(){

        HttpUtilslwl httpUtilslwl = new HttpUtilslwl();
        String url = "http://localhost:1234/index/say";

        Map<String,String> params = new HashMap<>();
        //params.put("name1","bruce1");
        params.put("name1","bruce2");
        params.put("name","bruce3");
        params.put("address","上海");
        params.put("age","15");

        HttpUtilslwl.HttpResp httpResp = httpUtilslwl.httpClientdoPost(url, params, StandardCharsets.UTF_8.name());
        //HttpUtilslwl.HttpResp httpResp = httpUtilslwl.httpClientdoGet(url, params, StandardCharsets.UTF_8.name());

        System.out.println(JSON.toJSONString(httpResp));

    }

    @Test
    public void loginTest() throws IOException {
        HttpUtilslwl httpUtilslwl = new HttpUtilslwl();

        String url = "http://localhost:8080/baoyu-financing/login";
        Map<String, String> params = new HashMap<>();
        params.put("username", "com123");
        params.put("password", "123456");

        HttpUtilslwl.HttpResp httpResp = httpUtilslwl.httpClientdoPost(url, params, StandardCharsets.UTF_8.name());
        System.out.println(JSON.toJSONString(httpResp));

        String url2 = "http://localhost:8080/baoyu-financing/index/listObject";
        String s = httpUtilslwl.httpdoGet(url2, null, null);
        System.out.println(JSON.toJSONString(s));

    }




}
