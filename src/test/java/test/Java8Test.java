package test;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.junit.Before;
import org.junit.Test;

import com.lwl.java8.chapter4.Dish;

public class Java8Test {
	
	List<Dish> menu;

	@Before
	public void before() {
		menu = Arrays.asList(new Dish("chicken", false, 800, Dish.Type.MEAT), new Dish("chicken", false, 700, Dish.Type.MEAT),
				new Dish("chicken", false, 400, Dish.Type.MEAT), new Dish("哈哈哈", true, 530, Dish.Type.OTHER),
				new Dish("wre", true, 350, Dish.Type.OTHER), new Dish("和和", true, 120, Dish.Type.OTHER),
				new Dish("蔬菜", true, 550, Dish.Type.OTHER), new Dish("chicken", false, 300, Dish.Type.FISH),
				new Dish("chicken", false, 450, Dish.Type.FISH));
	}
	
	@Test
	public void test1() {
		List<Integer> numbers = Arrays.asList(1,2,2,3,4,5,6,7,8);
		numbers.stream()
		.filter(i->i % 2 == 0)
		.distinct()
		.forEach(System.out::println);
	}
	
	@Test
	public void limitTest() {
		List<Dish> collect = menu.stream()
		.filter(d->d.getCalories() > 300)
		//.sorted(d.getCalories())
		.limit(3)
		.collect(Collectors.toList());
		
		for (Dish dish : collect) {
			System.out.println(dish.getCalories());
		}
	}
	
	@Test
	public void mapTest() {
		List<String> words = Arrays.asList("Java 8", "Lambdas", "In", "Action");
		 words.stream()
		.map(String::length)
		.collect(Collectors.toList())
		.forEach(System.out::println);
	}
	
	@Test
	public void flatMapTest() {
		List<String> words = Arrays.asList("Java 8", "Lambdas", "In", "Action");
		
		words.parallelStream()
		.map(w->w.split(""))
		.flatMap(Arrays::stream)
		.distinct()
		.collect(Collectors.toList())
		.forEach(System.out::print);
	}
	
	@Test
	public void matchTest() {
		if(menu.stream().anyMatch(Dish::isVegetarian)){
			System.out.println("The menu is (somewhat) vegetarian friendly!!");
		}

		boolean allMatch = menu.stream().allMatch(d->d.getCalories() < 500);
		System.out.println(allMatch);
		
		boolean noneMatch = menu.stream().noneMatch(
		   d -> d.getCalories() > 1000 && d.isVegetarian()
		);
		System.out.println(noneMatch);
	}
	
	@Test
	public void findTest(){
		Optional<Dish> any = menu.parallelStream()
		.filter(Dish::isVegetarian)
		.findAny();
		any.ifPresent(System.out::println);
		
		Optional<Dish> findFirst = menu.stream().filter(Dish::isVegetarian)
		.findFirst();
		
		findFirst.ifPresent(d->System.out.println(d.getName()));
	}
	
	@Test
	public void reduceTest() {
		//(1+100)×(100÷2)
		
		int[] num = new int[]{1,2,3,4,5,6,7};
		int temp = 1;
		for (int i : num) {
			temp = temp * i;
		}
		System.out.println(temp);
		
		int sum = Arrays.stream(num)
		.reduce(1,(a,b)-> a * b );
		System.out.println(sum);
		
		int reduce = Arrays.stream(num)
		.reduce(0,Integer::sum);
		System.out.println(reduce);
		
	    OptionalInt reduce2 = Arrays.stream(num)
				.reduce((a, b) -> (a + b));
	    reduce2.ifPresent(System.out::println);
	    
	    OptionalInt reduce3 = Arrays.stream(num)
	    		.reduce((a,b)-> a * b);
	    reduce3.ifPresent(System.out::println);
	    
	    OptionalInt reduce4 = Arrays.stream(num)
		.reduce(Integer::max);
	    reduce4.ifPresent(System.out::println);
	    
	    Optional<Integer> reduce5 = menu.stream().map(a->1).reduce(Integer::sum);
	    System.out.println(reduce5.get());
	    
	    LongStream longStream = Arrays.stream(num).asLongStream();
	    
	    //并行计算
	    long reduce6 = longStream.parallel().reduce(0, (a,b)->a+b);
	    System.out.println(reduce6);
	    
	    
	}

	private void test(){

		try {
			int a = 10 / 0;
		}catch (Exception e){
			System.out.println("异常发生");
			test();
		}


	}

	@Test
	public void dateTest(){

		Boolean b1 = null;
		Boolean b2 = null;

		//System.out.println(b1 ? "1" : "0");

		System.out.println(Boolean.FALSE.equals(b1));

	}
	
	
	
	
	

}
