package test;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author liwenlong - 2018/1/22 19:50
 */
public class Location {
    @JSONField(name = "_id")
    private Long id;

    @JSONField(name = "_location")
    private String location;

    @JSONField(name = "_name")
    private String name;

    @JSONField(name = "_address")
    private String address;

    @JSONField(name = "deviceId")
    private String deviceId;

    @JSONField(name = "_createtime")
    private String createtime;

    @JSONField(name = "_updatetime")
    private String updatetime;

    @JSONField(name = "_province")
    private String province;

    @JSONField(name = "_city")
    private String city;

    @JSONField(name = "_district")
    private String district;

    @JSONField(name = "_image")
    private List<String> image;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }
}
