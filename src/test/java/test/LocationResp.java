package test;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author liwenlong - 2018/1/22 19:35
 */
public class LocationResp {
    /**
     * 值为0或1 1：成功；0：失败
     */
    private int status;
    /**
     * status = 1，info返回“ok”
     */
    private String info;
    /**
     * 返回结果总数目
     */
    private String count;
    private String infocode;
    private List<Location> datas;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getInfocode() {
        return infocode;
    }

    public void setInfocode(String infocode) {
        this.infocode = infocode;
    }

    public List<Location> getDatas() {
        return datas;
    }

    public void setDatas(List<Location> datas) {
        this.datas = datas;
    }
}
