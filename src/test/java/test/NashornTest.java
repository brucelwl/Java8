package test;

import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by liwenlong on 2018/1/3 14:00 <br/>
 */
public class NashornTest {

    @Test
    public void nashornTest() throws ScriptException, FileNotFoundException, NoSuchMethodException {
        NashornScriptEngineFactory nashornScriptEngineFactory = new NashornScriptEngineFactory();
        NashornScriptEngine scriptEngine = (NashornScriptEngine) nashornScriptEngineFactory.getScriptEngine();
        //scriptEngine.eval("alert('Hello World!');");

        FileReader fileReader = new FileReader("D:\\Workspaces\\IdeaProjects\\Java8\\src\\main\\webapp\\js\\index.js");
        scriptEngine.eval(fileReader);

        Object fun1 = scriptEngine.invokeFunction("fun1", "liwenlong");
        System.out.println(fun1);

        Object fun2 = scriptEngine.invokeFunction("fun2", "liwenlong");
        System.out.println(fun2);

    }

    @Test
    public void jsTest() throws ScriptException, FileNotFoundException, NoSuchMethodException {
        NashornScriptEngineFactory nashornScriptEngineFactory = new NashornScriptEngineFactory();
        NashornScriptEngine scriptEngine = (NashornScriptEngine) nashornScriptEngineFactory.getScriptEngine();
        //scriptEngine.eval("alert('Hello World!');");

        //FileReader fileReaderJquery = new FileReader("D:\\Workspaces\\IdeaProjects\\Java8\\src\\main\\webapp\\js\\jquery-1.9.1.min.js");
        //FileReader fileReaderRsa = new FileReader("D:\\Workspaces\\IdeaProjects\\Java8\\src\\main\\webapp\\js\\jsbn.js");
        FileReader fileReaderRsa = new FileReader("D:\\Workspaces\\IdeaProjects\\Java8\\src\\main\\webapp\\js\\union-sm2-mini-1.0.js");
        FileReader fileReader = new FileReader("D:\\Workspaces\\IdeaProjects\\Java8\\src\\main\\webapp\\js\\login.js");

        //scriptEngine.eval(fileReaderJquery);
        scriptEngine.eval(fileReaderRsa);
        scriptEngine.eval(fileReader);

        Object fun1 = scriptEngine.invokeFunction("doEncryptRSA", "rsaPub","liwenlong");
        System.out.println(fun1);


    }


    /**
     * 判断是否是数字
     */
    public boolean isNumber(String number) {
        Pattern pattern = Pattern.compile("^\\d+(\\.\\d+)?");
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }



}
