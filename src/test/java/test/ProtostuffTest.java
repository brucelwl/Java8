package test;

import com.lwl.utils.ProtostuffUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.LongSummaryStatistics;

/**
 * @author liwenlong - 2018/3/12 11:05
 */
public class ProtostuffTest {

    public static void main(String[] args) throws InterruptedException {

        serialUser();

        int count = 100;
        List<Long> times = new ArrayList<>(count);
        //List<byte[]> datas = new ArrayList<>(count);
        for (int j = 0; j < count; j++) {
            long start = System.currentTimeMillis();
            for (int i = 0; i < 100; i++) {
                //datas.add(serial());
                serial();
                System.gc();
                //Thread.sleep(5);
            }
            long temp = System.currentTimeMillis() - start;
            //System.out.println(temp);
            times.add(temp);
        }

        serialUser();

        LongSummaryStatistics longSummaryStatistics = times.stream().mapToLong(a -> a).summaryStatistics();
        System.out.println(longSummaryStatistics);


    }

    private static byte[] serial() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName("Welcome to Netty 上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园");
        userInfo.setUserId(100);
        userInfo.setAddress("上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园");
        //System.out.println("----------------------------");
        byte[] protobytes = ProtostuffUtil.serializer(userInfo);
        //System.out.println("the protostuff serializable length is:" + protobytes.length);
        UserInfo user = ProtostuffUtil.deserializer(protobytes, UserInfo.class);
        //System.out.println(user);
        return protobytes;
    }

    private static byte[] serialUser() {
        User user = new User();
        user.setUserName("Welcome to Netty 上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园");
        user.setUserId(100);
        user.setAddress("上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园上海市浦东新区陆家嘴软件园");
        //System.out.println("----------------------------");
        byte[] protobytes = ProtostuffUtil.serializer(user);
        //System.out.println("the protostuff serializable length is:" + protobytes.length);
        User user2 = ProtostuffUtil.deserializer(protobytes, User.class);
        //System.out.println(user);
        return protobytes;
    }


}
