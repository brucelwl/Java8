package test;

import com.alibaba.fastjson.JSON;
import com.lwl.utils.ProtostuffUtil;
import org.junit.Test;
import org.msgpack.MessagePack;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;

/**
 * @author liwenlong - 2018/3/7 16:58
 */
public class SerialiTest {

    @Test
    public void serialiSizeTest() throws Exception {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName("Welcome to Netty");
        userInfo.setUserId(100);
        userInfo.setAddress("上海市浦东新区陆家嘴软件园");

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(userInfo);
        os.flush();
        os.close();
        byte[] bytes = bos.toByteArray();
        System.out.println("the jdk serializable length is:" + bytes.length);
        bos.close();
        System.out.println("----------------------------");
        System.out.println("the byte array serializable length is:" + userInfo.codec().length);

        System.out.println("----------------------------");
        byte[] protobytes = ProtostuffUtil.serializer(userInfo);
        System.out.println("the protostuff serializable length is:" + protobytes.length);
        User user = ProtostuffUtil.deserializer(protobytes, User.class);
        System.out.println(user);

        System.out.println("----------------------------");
        String s = JSON.toJSONString(userInfo);
        System.out.println("the json serializable length is:" + s.getBytes().length);

        System.out.println("----------------------------");
        MessagePack pack = new MessagePack();
        byte[] packbytes = pack.write(userInfo);
        System.out.println("the messagePack serializable length is:" + packbytes.length);
        User read = pack.read(packbytes, User.class);
        System.out.println(read);
    }

    @Test
    public void PerformTest() throws Exception {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName("Welcome to Netty");
        userInfo.setUserId(100);
        int loop = 5000000;
        long start = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bos);
            os.writeObject(userInfo);
            os.flush();
            os.close();
            byte[] bytes = bos.toByteArray();
            bos.close();
        }
        long endTime = System.currentTimeMillis() - start;
        System.out.println("the jdk serializable cost time:" + endTime);

        start = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            byte[] codec = userInfo.codec();
        }
        System.out.println("the byte array serializable cost time:" + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            byte[] serializer = ProtostuffUtil.serializer(userInfo);
        }
        System.out.println("the protostuff serializable cost time:" + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        for (int i = 0; i < loop; i++) {
            byte[] bytes = JSON.toJSONString(userInfo).getBytes();
        }
        System.out.println("the JSON serializable cost time:" + (System.currentTimeMillis() - start));

    }

    @Test
    public void constTest() throws NoSuchFieldException, IllegalAccessException {

        System.out.println(User.aa == UserInfo.aa);
        User user = new User();
        UserInfo userInfo = new UserInfo();

        System.out.println(user.getUserName() == userInfo.getUserName());

        user.setUserName("hhhh");
        userInfo.setUserName("hhhh");
        System.out.println(user.getUserName() == userInfo.getUserName());

        System.out.println(user.getUserName() == user.getAddress());

        System.out.println(new AA().aa == new BB().bb);
        System.out.println(new AA().aa == AA.bb);
        System.out.println(AA.cc == BB.cc);

        System.out.println(User.getCc() == UserInfo.getCc());
        Field cc1 = user.getClass().getDeclaredField("cc");
        Field cc2 = userInfo.getClass().getDeclaredField("cc");
        cc1.setAccessible(true);
        cc2.setAccessible(true);
        Object o = cc1.get(user);
        Object o1 = cc2.get(userInfo);
        System.out.println(o == o1);
        System.out.println(o+" "+ o1);

        cc1.set(user,"aaa");
        cc2.set(userInfo,"aaa");

        System.out.println(User.getCc() == UserInfo.getCc());
        System.out.println(User.getCc());

    }

    static class AA {
        private String aa = "aaa";
        public static String bb = "aaa";
        private static String cc = "ccc";
    }

    static class BB {
        private String bb = "aaa";
        private static String cc = "ccc";
    }

}
