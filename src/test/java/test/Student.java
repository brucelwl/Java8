package test;

import com.alibaba.fastjson.JSON;

import java.util.Vector;

/**
 * @author liwenlong - 2018/1/22 15:43
 */
public class Student implements Cloneable {
    private int id;
    private String name;
    private Vector<String> courses;

    public Student(){
        System.out.println("Student created");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector<String> getCourses() {
        return courses;
    }

    public void setCourses(Vector<String> courses) {
        this.courses = courses;
    }

    /**
     * 浅拷贝
     */
    public Student newInstance(){
        try {
            return (Student) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args) {
        Student student1 = new Student();

        Vector<String> course = new Vector<>();
        course.add("java");

        student1.setCourses(course);
        student1.setId(11);
        student1.setName("11111111");

        Student student = student1.newInstance();
        Vector<String> courses = student.getCourses();
        System.out.println(JSON.toJSONString(student));

        double count = 4.0 * 0.6 * (1 + 2000.0 / 1000.0);
        System.out.println(count);

    }



}
